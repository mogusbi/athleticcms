// Delete something
function confirmDelete(id) {
	if (confirm('Are you sure you wish to delete this?'))
		document.getElementById(id).submit();
		
	event.returnValue = false;
	return false;
}

// JavaScript onLoad
$(function(){
	// Set width on actions column
	$('th:contains(Actions)').addClass('onetwofivewide');
	
	// Remove margin on last element in pagination area
	$('.pagination a:last-child').removeClass('marfivert');
	
	// Focus on first input field on a page if there's one there
	$("form:not(.filter) :input:visible:enabled:first").focus();
	
});

// Form validation
$(function(){
	$('form').submit(function(e) {
		var doSubmit = true;
		
		// Check for empty values
		$('form input[data-validation="true"]').each(function() {
			if ($(this).val() == '') {
				$(this).closest('.inputwrap').children('.error').removeClass('dspnone').html('This field is required and cannot be left empty');
				doSubmit = false;
			} else
				$(this).closest('.inputwrap').children('.error').addClass('dspnone');
		});
		
		// Select menu duplicate validation
		var values = [];
		
		$('form select[data-validation="true"]').each(function() {
			values.push($(this).val());
		});
		
		values.sort();
		var last = values[0];
		
		for (var i = 1; i < values.length; i++) {
			if (values[i] == last) {
				alert('Error: Duplicate value detected');
				doSubmit = false;
				break;
			}
			
			last = values[i];
		}
		
		
		// Email validation
		var emailRegEx = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

		$('form input[type=email]').each(function() {
			if (!emailRegEx.test($(this).val())) {
				$(this).closest('.inputwrap').children('.error').removeClass('dspnone').html('Invalid email address');
				doSubmit = false;
			} else
				$(this).closest('.inputwrap').children('.error').addClass('dspnone');
		});
		
		if(doSubmit) return true;
		else e.preventDefault();
	});
});