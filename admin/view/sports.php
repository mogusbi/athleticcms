<?php

	$HTML->adminLink('sports/add', 'Add new sport', 'button left');
	$HTML->adminPagination('sports', end($url), $controller[1]);
	$HTML->clear();
	
?>
<table>
	<tr>
		<th>Sport</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?=$row['sport']; ?></td>
		<td>
			<?php $HTML->adminLink('sports/edit/'.$row['sport_id'], 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/delete/', $row['sport_id'], 'Delete', 'sport_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile ?>
</table>