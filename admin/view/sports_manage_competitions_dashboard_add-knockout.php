<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
		
			$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'Back', 'button right');
		
			$HTML->tag('h1', $page['competition'].' '.$page['season'], 'fsth');
			$HTML->tag('h2', $page['title'], 'fsfe');
			
			$HTML->clear();

			$FORM->startForm();
			$FORM->autocomplete($page['autocomplete'], 'inputstagename');
			$FORM->inputBox('stage_name', 'text');
			$FORM->dropdownMenu('format', 'Format', array(0,1), array('Aggregate based', 'Best of series'));
			$FORM->inputBox('round', 'text');
			$FORM->hidden('comp_id', $url[4]);
			$FORM->endForm('Add new knockout stage');
		
		else:	
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>