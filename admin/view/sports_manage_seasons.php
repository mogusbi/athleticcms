<?php

	$HTML->adminLink('sports/manage/'.$url[2].'/seasons/add', 'Add new season', 'button left');
	$HTML->adminLink('sports/manage/'.$url[2], 'Back', 'button right');
	$HTML->clear();
	$HTML->adminPagination('sports/manage/'.$url[2].'/seasons', end($url), $controller[1]);
	$HTML->clear();
	
?>
<table>
	<tr>
		<th>Season</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?=$row['season']; ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/seasons/edit/'.$row['season_id'], 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/seasons/delete/', $row['season_id'], 'Delete', 'season_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile ?>
</table>