<?php

if ($MYSQL->countRows($controller)):
	while($row = $MYSQL->FetchArray($controller)):
		$HTML->adminLink('sports/manage/'.$url[2].'/seasons', 'Back', 'button right');

		$HTML->tag('h1', $page['title']);
		
		$FORM->startForm();
		$FORM->inputBox('season', 'text', $row['season']);
		$FORM->hidden('season_id', $row['season_id']);
		$FORM->endForm('Edit season');
	endwhile;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>