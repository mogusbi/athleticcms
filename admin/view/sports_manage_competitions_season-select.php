<?php

if ($MYSQL->countRows($check)):

	$HTML->tag('h1', $page['title'], 'left');
	
	$HTML->adminLink('sports/manage/'.$url[2].'/competitions', 'Back', 'button right');
	
	$HTML->clear();
	
	$HTML->adminPagination('sports/manage/'.$url[2].'/competitions', end($url), $controller[1]);
	
	$HTML->clear();

?>
<table>
	<tr>
		<th>Select season</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$row['season_id'], $row['season'], 'link'); ?></td>
		<td><?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$row['season_id'].'/reset', $row['season_id'], 'Reset', 'season_id', 'link'); ?></td>
	</tr>
	<?php endwhile ?>
</table>
<?php

else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>