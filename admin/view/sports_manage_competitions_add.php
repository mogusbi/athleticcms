<?php

if ($MYSQL->countRows($controller)):
	$HTML->adminLink('sports/manage/'.$url[2].'/competitions', 'Back', 'button right');
	
	$HTML->tag('h1', $page['title']);
	
	$FORM->startForm();
	$FORM->inputBox('competition_name', 'text');
	$FORM->inputBox('participants', 'text');
	$FORM->inputBox('bonus_points', 'text', '1');
	$FORM->hidden('sport_id', $url[2]);
	$FORM->endForm('Add new competition');
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>