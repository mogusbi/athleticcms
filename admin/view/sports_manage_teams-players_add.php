<?php

if ($MYSQL->countRows($controller)):
	$HTML->adminLink('sports/manage/'.$url[2].'/teams-players', 'Back', 'button right');

	$HTML->tag('h1', $page['title']);
	
	$FORM->startForm();
	$FORM->inputBox('name', 'text');
	$FORM->hidden('sport_id', $url[2]);
	$FORM->endForm('Add new team/player');
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>