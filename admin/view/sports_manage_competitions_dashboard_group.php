<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
			
				$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'Back', 'button right');
				
				$HTML->tag('h1', $page['competition'].' '.$page['season'], 'fsth');
				$HTML->tag('h2', $page['title'], 'fsfe');
				
				$HTML->clear();
				
				$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8].'/add', 'Add new tie', 'button left martwenrt');
				
				if ($MYSQL->countRows($controller[0]) == 0):
					$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8].'/generate', 'Generate ties', 'button left');
				endif;
				
				$HTML->clear();
				
				$count = 0;
				
?>
<table>
	<tr>
		<th colspan="2">Standings</th>
		<th class="txtcen">PLD</th>
		<th class="txtcen">W</th>
		<th class="txtcen">D</th>
		<th class="txtcen">L</th>
		<th class="txtcen">+</th>
		<th class="txtcen">-</th>
		<th class="txtcen">+/-</th>
		<th class="txtcen">AVG +</th>
		<th class="txtcen">AVG -</th>
		<th class="txtcen">PTS</th>
	</tr>
<?php 
	while($row = $MYSQL->FetchArray($controller[1])):

	$count++;
?>
	<tr>
		<td class="txtcen"><?=$count; ?></td>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_id'].'/dashboard/'.$url[6], $row['team'], 'link'); ?></td>
		<td class="txtcen"><?=$row['pld']; ?></td>
		<td class="txtcen"><?=$row['wins']; ?></td>
		<td class="txtcen"><?=$row['draws']; ?></td>
		<td class="txtcen"><?=$row['defeats']; ?></td>
		<td class="txtcen"><?=$row['score_for']; ?></td>
		<td class="txtcen"><?=$row['score_against']; ?></td>
		<td class="txtcen"><?=$row['score_difference']; ?></td>
		<td class="txtcen"><?=$row['average_score_for']; ?></td>
		<td class="txtcen"><?=$row['average_score_against']; ?></td>
		<td class="txtcen"><?=$row['pts']; ?></td>
	</tr>
<?php endwhile; ?>
</table>
<table>
	<tr>
		<th colspan="3">Ties</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr<?php if ($row['fixture_check'] == '0') echo(' class="bglight"'); ?>>
		<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a_id'].'/dashboard/'.$url[6], $row['team_a'], 'link'); ?></td>
		<td class="txtcen">-</td>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b_id'].'/dashboard/'.$url[6], $row['team_b'], 'link'); ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8].'/tie/'.$row['tie_id'], 'View', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8].'/tie/'.$row['tie_id'].'/delete', $row['tie_id'], 'Delete', 'tie_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<?php
				
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
		
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>