<?php

$HTML->adminLink('users', 'Back', 'button right');

$HTML->tag('h1', $page['title']);

$FORM->startForm();
$FORM->inputBox('first_name', 'text');
$FORM->inputBox('surname', 'text');
$FORM->inputBox('email', 'email');
$FORM->inputBox('password', 'password');
$FORM->endForm('Add new user');

?>