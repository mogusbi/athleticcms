<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if($MYSQL->countRows($controller[1]) || $MYSQL->countRows($controller[2])):
			
			if (isset($url[7])):
				$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$url[4].'/dashboard/'.$url[6], 'Back', 'button right');
			else:
				$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$url[4].'/season-select', 'Back', 'button right');
			endif;
			
			$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/edit/'.$url[4], 'Edit', 'button right martwenrt');
			
			$HTML->tag('h1', $page['title']);
			
			while($row = $MYSQL->FetchArray($check[1])):
				$HTML->tag('h2', $page['competition'].' '.$row['season']);
			endwhile;
			
			$HTML->clear();
			
			if (!isset($url[7])):
?>
<table>
	<tr>
		<th>Participating in</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[3])): ?>
	<tr>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$url[4].'/dashboard/'.$url[6].'/'.$row['comp_id'], $row['competition_name'], 'link'); ?></td>
	</tr>
	<?php endwhile; ?>
</table>
<div class="clear"></div>
<?php
			endif;
			
			if ($MYSQL->countRows($controller[1])):
?>
<div class="left border-box fiftypcwide padtenrt">
	<table>
		<tr>
			<th colspan="3">Next <?=$MYSQL->countRows($controller[1]); ?> fixture(s)</th>
		</tr>
		<?php while($row = $MYSQL->FetchArray($controller[1])): ?>
		<tr>
			<td>
				<?php
					if ($row['team_a'] == $url[4]):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b'].'/dashboard/'.$url[6], $row['team_b_name'], 'link');
						$HTML->tag('span', ' (h)', 'bold');
					elseif($row['team_b'] == $url[4]):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a'].'/dashboard/'.$url[6], $row['team_a_name'], 'link');
						$HTML->tag('span', ' (a)', 'bold');
					endif;
				?>
			</td>
			<td><?php $HTML->convertDate($row['fixture_date']); ?></td>
			<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$row['comp_id'].'/dashboard/'.$url[6].'/'.$row['format'].'/'.$row['rid'].'/tie/'.$row['tie_id'].'/fixture/'.$row['fx_id'].'/edit', 'Edit', 'link'); ?></td>
		</tr>
		<?php endwhile; ?>
	</table>
</div>
<?php
			
			endif;
			
			if ($MYSQL->countRows($controller[2])):
?>
<div class="right border-box fiftypcwide padtenlt">
	<table>
		<tr>
			<th colspan="4">Last <?=$MYSQL->countRows($controller[2]); ?> result(s)</th>
		</tr>
		<?php while($row = $MYSQL->FetchArray($controller[2])): ?>
		<tr>
			<td>
				<?php
					if ($row['team_a'] == $url[4]):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b'].'/dashboard/'.$url[6], $row['team_b_name'], 'link');
						$HTML->tag('span', ' (h)', 'bold');
					elseif($row['team_b'] == $url[4]):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a'].'/dashboard/'.$url[6], $row['team_a_name'], 'link');
						$HTML->tag('span', ' (a)', 'bold');
					endif;
				?>
			</td>
			<td class="txtcen"><?=$row['team_a_score']; ?> - <?=$row['team_b_score']; ?></td>
			<td><?php $HTML->convertDate($row['fixture_date']); ?></td>
			<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$row['comp_id'].'/dashboard/'.$url[6].'/'.$row['format'].'/'.$row['rid'].'/tie/'.$row['tie_id'].'/fixture/'.$row['fx_id'].'/edit', 'Edit', 'link'); ?></td>
		</tr>
		<?php endwhile; ?>
	</table>
</div>
<?php
			endif;
			
			if ($page['pld'] != 0):
?>
<div class="left border-box fiftypcwide padtenrt">
<h2 class="bgblue font-white fsei bold padtwenlt padtwenrt lnfourty ubuntu martwentp">Season form</h2>
	<div id="form" class="boderbtsolid borderwidthfive borderblue"></div>
	<script type="text/javascript">
	$(function() {
		Highcharts.setOptions({
     		colors: ['#165f0c', '#eeeeee', '#b02017']
		});
		
		$('#form').highcharts({
			chart: {
				type: 'pie'
			},
			title: {
				text: null
			},
			tooltip: {
				formatter: function() {
					return '<b>'+ this.point.name +'</b>: '+ this.y + ' ('+ Math.round(this.point.percentage) +' %)';
				}
			},
			plotOptions: {
                pie: {
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
			series: [{
				data: [
					['Wins', <?=$page['wins']; ?>],
					['Draws', <?=$page['draws']; ?>],
					['Defeats', <?=$page['defeats']; ?>]
				]
			}]
		});
	});
	</script>
</div>
<div class="right border-box fiftypcwide padtenlt">
	<table>
		<tr>
			<th></th>
			<th class="txtcen">Home</th>
			<th class="txtcen">Away</th>
			<th class="txtcen">Overall</th>
		</tr>
		<tr class="boderbtsolid borderwidthfive borderblue">
			<td class="bold">Matches played</td>
			<td class="txtcen"><?=$page['home_pld']; ?></td>
			<td class="txtcen"><?=$page['away_pld']; ?></td>
			<td class="txtcen"><?=$page['pld']; ?></td>
		</tr>
		<tr>
			<td class="bold">Wins</td>
			<td class="txtcen"><?=$page['home_wins']; ?></td>
			<td class="txtcen"><?=$page['away_wins']; ?></td>
			<td class="txtcen"><?=$page['wins']; ?></td>
		</tr>
		<tr>
			<td class="bold">Draws</td>
			<td class="txtcen"><?=$page['home_draws']; ?></td>
			<td class="txtcen"><?=$page['away_draws']; ?></td>
			<td class="txtcen"><?=$page['draws']; ?></td>
		</tr>
		<tr class="boderbtsolid borderwidthfive borderblue">
			<td class="bold">Defeats</td>
			<td class="txtcen"><?=$page['home_defeats']; ?></td>
			<td class="txtcen"><?=$page['away_defeats']; ?></td>
			<td class="txtcen"><?=$page['defeats']; ?></td>
		</tr>
		<tr>
			<td class="bold">Win %</td>
			<td class="txtcen"><?=round(($page['home_wins']/$page['home_pld'])*100); ?>%</td>
			<td class="txtcen"><?=round(($page['away_wins']/$page['away_pld'])*100); ?>%</td>
			<td class="txtcen"><?=round(($page['wins']/$page['pld'])*100); ?>%</td>
		</tr>
		<tr>
			<td class="bold">Draw %</td>
			<td class="txtcen"><?=round(($page['home_draws']/$page['home_pld'])*100); ?>%</td>
			<td class="txtcen"><?=round(($page['away_draws']/$page['away_pld'])*100); ?>%</td>
			<td class="txtcen"><?=round(($page['draws']/$page['pld'])*100); ?>%</td>
		</tr>
		<tr class="boderbtsolid borderwidthfive borderblue">
			<td class="bold">Defeat %</td>
			<td class="txtcen"><?=round(($page['home_defeats']/$page['home_pld'])*100); ?>%</td>
			<td class="txtcen"><?=round(($page['away_defeats']/$page['away_pld'])*100); ?>%</td>
			<td class="txtcen"><?=round(($page['defeats']/$page['pld'])*100); ?>%</td>
		</tr>
		<tr>
			<td class="bold">For</td>
			<td class="txtcen"><?=$page['home_score_for']; ?></td>
			<td class="txtcen"><?=$page['away_score_for']; ?></td>
			<td class="txtcen"><?=$page['score_for']; ?></td>
		</tr>
		<tr class="boderbtsolid borderwidthfive borderblue">
			<td class="bold">Against</td>
			<td class="txtcen"><?=$page['home_score_against']; ?></td>
			<td class="txtcen"><?=$page['away_score_against']; ?></td>
			<td class="txtcen"><?=$page['score_against']; ?></td>
		</tr>
		<tr>
			<td class="bold">AVG +</td>
			<td class="txtcen"><?=round(($page['home_score_for']/$page['home_pld']), 1); ?></td>
			<td class="txtcen"><?=round(($page['away_score_for']/$page['away_pld']), 1); ?></td>
			<td class="txtcen"><?=round(($page['score_for']/$page['pld']), 1); ?></td>
		</tr>
		<tr>
			<td class="bold">AVG -</td>
			<td class="txtcen"><?=round(($page['home_score_against']/$page['home_pld']), 1); ?></td>
			<td class="txtcen"><?=round(($page['away_score_against']/$page['away_pld']), 1); ?></td>
			<td class="txtcen"><?=round(($page['score_against']/$page['pld']), 1); ?></td>
		</tr>
	</table>
</div>
<?php
			endif;
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>