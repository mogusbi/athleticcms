<?php

if ($MYSQL->countRows($controller)):
	while($row = $MYSQL->FetchArray($controller)):
		$HTML->adminLink('sports/manage/'.$url[2].'/competitions', 'Back', 'button right');
		
		$HTML->tag('h1', $page['title']);
		
		$FORM->startForm();
		$FORM->inputBox('competition_name', 'text', $row['competition_name']);
		$FORM->inputBox('participants', 'text', $row['participants']);
		$FORM->inputBox('bonus_points', 'text', $row['bonus_points']);
		$FORM->hidden('comp_id', $row['comp_id']);
		$FORM->endForm('Edit competition');
	endwhile;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>