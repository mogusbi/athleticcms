<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
				if ($MYSQL->countRows($controller[0])):
				
					$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10], 'Back', 'button right');
										
					$HTML->tag('h1', $page['competition'].' '.$page['season'].' '.strtolower($page['stage']).': '.$page['tie'], 'fsth');
					$HTML->tag('h2', $page['title'], 'fsfe');
					
					$HTML->clear();
					
					$FORM->startForm();
					$FORM->dropdownMenu('team_a', 'Participant 1 (home)', $page['team_id'], $page['team_name']);
					$FORM->score('team_a_score');
					$FORM->dropdownMenu('team_a_bonus', 'Award bonus point(s)', array(0,1), array('No', 'Yes'), NULL, FALSE);
					$FORM->dropdownMenu('team_b', 'Participant 2 (away)', $page['team_id'], $page['team_name']);
					$FORM->score('team_b_score');
					$FORM->dropdownMenu('team_b_bonus', 'Award bonus point(s)', array(0,1), array('No', 'Yes'), NULL, FALSE);
					$FORM->datePicker('fixture_date');
					$FORM->hidden('tie_id', $url[10]);
					$FORM->endForm('Add new fixture');

				else:
					$HTML->errorMsg('Ooops, something seems to have gone wrong...');
				endif;
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
			
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>