<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
				$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8], 'Back', 'button right');
		
				$HTML->tag('h1', $page['competition'].' '.$page['season'], 'fsth');
				$HTML->tag('h2', $page['title'], 'fsfe');
				
				$HTML->clear();
		
				$FORM->startForm();
				$FORM->dropdownMenu('team_a', 'Participant 1', $page['team_id'], $page['team_name']);
				$FORM->dropdownMenu('team_b', 'Participant 2', $page['team_id'], $page['team_name']);
				$FORM->hidden('comp_id', $url[4]);
				$FORM->hidden('season_id', $url[6]);
				$FORM->hidden('rid', $url[8]);
				$FORM->endForm('Add new tie');
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
			
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>