<?php

if ($MYSQL->countRows($controller)):
	while($row = $MYSQL->FetchArray($controller)):
		$HTML->adminLink('sports/manage/'.$url[2].'/teams-players', 'Back', 'button right');
		
		$HTML->tag('h1', $page['title']);
		
		$FORM->startForm();
		$FORM->inputBox('name', 'text', $row['name']);
		$FORM->hidden('sport_id', $row['sport_id']);
		$FORM->hidden('team_id', $row['team_id']);
		$FORM->endForm('Edit team/player');
	endwhile;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>