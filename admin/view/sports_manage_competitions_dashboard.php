<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		
		$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/season-select', 'Back', 'button right');
		
		$HTML->tag('h1', $page['competition'].' '.$page['season'], 'fsth');
			
		// Add participants
		if ($MYSQL->countRows($check[2]) == 0):

			$HTML->tag('h2', 'Select participants', 'fsfe');
			
			$HTML->clear();
			
			$FORM->startForm();

			for($i = 1; $i <= $page['participants']; $i++):
				$FORM->dropdownMenu('team_id[]', 'Team/player '.$i, $page['team_ids'], $page['team_names']);
			endfor;
			
			$FORM->endForm('Next');
		else:
		// Competition dashboard
			
			$HTML->tag('h2', 'Dashboard', 'fsfe');
			
			$HTML->clear();
			
			$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/add-knockout', 'Add new knockout stage', 'button left martwenrt');
			$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/add-group', 'Add new group stage/league', 'button left martwenrt');
			
			$HTML->clear();
?>
<table>
	<tr>
		<th>Select round</th>
		<th>Actions</th>
	</tr>
	<?php
		while($row = $MYSQL->FetchArray($controller[1])):
			if ($row['format'] == '2')
				$page['format'] = 'group';
			else
				$page['format'] = 'knockout';
	?>
	<tr>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/'.$page['format'].'/'.$row['rid'], $row['stage_name'], 'link'); ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/'.$page['format'].'/'.$row['rid'].'/edit', 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/'.$page['format'].'/'.$row['rid'].'/delete', $row['rid'], 'Delete', 'rid', 'link'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<?php
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>