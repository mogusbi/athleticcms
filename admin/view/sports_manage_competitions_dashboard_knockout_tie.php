<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):	
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
				if ($MYSQL->countRows($controller[0])):
			
					$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8], 'Back', 'button right');
					
					$HTML->tag('h1', $page['competition'].' '.$page['season'].' '.strtolower($page['stage']), 'fsth');
					$HTML->tag('h2', $page['title'], 'fsfe');
					
					$HTML->clear();
					
					$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10].'/add', 'Add new fixture', 'button left martwenrt');
					$HTML->clear();
?>
<table>
	<tr>
		<th colspan="3"></th>
		<th>Date</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[1])): ?>
	<tr>
		<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a'].'/dashboard/'.$url[6], $row['team_a_name'], 'link'); ?></td>
		<td class="txtcen">
			<?php
				if (!empty($row['team_a_bonus']))
					echo ('* ');
					
				echo ($row['team_a_score'].' - '.$row['team_b_score']);
				
				if (!empty($row['team_b_bonus']))
					echo (' *');
			?>
		</td>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b'].'/dashboard/'.$url[6], $row['team_b_name'], 'link'); ?></td>
		<td><?php $HTML->convertDate($row['fixture_date']); ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10].'/fixture/'.$row['fx_id'].'/edit', 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10].'/fixture/'.$row['fx_id'].'/delete', $row['fx_id'], 'Delete', 'fx_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<p class="martwentp">* Denotes the award of bonus point(s)</p>
<?php			
				else:
					$HTML->errorMsg('Ooops, something seems to have gone wrong...');
				endif;
				
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
			
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>