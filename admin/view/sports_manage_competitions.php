<?php

if ($MYSQL->countRows($check)):

	$HTML->adminLink('sports/manage/'.$url[2].'/competitions/add', 'Add new competition', 'button left');
	$HTML->adminLink('sports/manage/'.$url[2], 'Back', 'button right');
	
	$HTML->clear();
	
	$HTML->adminPagination('sports/manage/'.$url[2].'/competitions', end($url), $controller[1]);
	
	$HTML->clear();

?>
<table>
	<tr>
		<th>Select competition</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$row['comp_id'].'/season-select', $row['competition_name'], 'link'); ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/edit/'.$row['comp_id'], 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/delete/', $row['comp_id'], 'Delete', 'comp_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile ?>
</table>
<?php

else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;
?>