<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?=$page['title']; ?> &laquo; AthleticCMS</title>

<!-- Styles -->
<link href="<?=$CONF['url']; ?>common/admin.css" rel="stylesheet" type="text/css" />
<!-- Styles -->

<!-- Icons -->
<link href="<?=$CONF['url']; ?>admin/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- Icons -->

<!-- JavaScript -->
<script src="<?=$CONF['url']; ?>common/jquery.min.js" type="text/javascript"></script>
<script src="<?=$CONF['url']; ?>common/admin.js" type="text/javascript"></script>
<!-- JavaScript -->
</head>

<body class="bglighter">
<!-- Header -->
<div class="bgdark">
	<div class="header onetwofivehigh padtwenlt padtwenrt">
		<a href="<?=$CONF['admin']; ?>login/" class="lnonetwofive font-white fsfe cabin semi-bold notransition sixhundredwide dspblock marginauto txtcen dspblock padtwenlt padtwenrt">Athletic<span class="fsth font-blue">cms</span></a>
	</div>
</div>
<!-- Header -->

<!-- Form -->
<div class="posrel">
	<div class="sixhundredwide bgwhite padtwen posabs topminustwen posxcen sixfourtycen">
		<?php
			if (isset($_SESSION['msg']))
				$HTML->msg($_SESSION['msg']);
				
			if (isset($_SESSION['error']))
				$HTML->errorMsg($_SESSION['error']);

			$FORM->startForm();
			$FORM->inputBox('email', 'email', $_POST['email']);
			$FORM->inputBox('password', 'password');
			$FORM->hidden('location', $CONF['admin'].implode('/', $url).'/');
			$FORM->endForm('Log in');
		?>
	</div>
</div>
<!-- Form -->
</body>
</html>
<?php
	if (isset($_SESSION['msg']))
		unset($_SESSION['msg']);
		
	if (isset($_SESSION['error']))
		unset($_SESSION['error']);
?>