<?php

if ($MYSQL->countRows($controller)):
	while($row = $MYSQL->FetchArray($controller)):
		$HTML->adminLink('sports', 'Back', 'button right');
		
		$HTML->tag('h1', $page['title']);
		
		$FORM->startForm();
		$FORM->inputBox('sport', 'text', $row['sport']);
		$FORM->hidden('sport_id', $row['sport_id']);
		$FORM->endForm('Edit sport');
	endwhile;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>