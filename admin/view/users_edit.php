<?php

if ($MYSQL->countRows($controller)):
	while($row = $MYSQL->FetchArray($controller)):
		$HTML->adminLink('users', 'Back', 'button right');
		
		$HTML->tag('h1', $page['title']);
		
		$FORM->startForm();
		$FORM->inputBox('first_name', 'text', $row['first_name']);
		$FORM->inputBox('surname', 'text', $row['surname']);
		$FORM->inputBox('email', 'email', $row['email']);
		$FORM->hidden('user_id', $row['user_id']);
		$FORM->endForm('Edit user');
	endwhile;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>