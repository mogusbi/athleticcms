<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
				if ($MYSQL->countRows($controller[0])):
					if ($MYSQL->countRows($controller[1])):
					
						while($row = $MYSQL->FetchArray($controller[1])):
				
							$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8].'/tie/'.$url[10], 'Back', 'button right');
							
							$HTML->tag('h1', $page['competition'].' '.$page['season'].': '.$page['tie'], 'fsth');
							$HTML->tag('h2', $page['title'], 'fsfe');
							
							$HTML->clear();
							
							$FORM->startForm();

							$HTML->tag('p', 'Participant 1 (home):', 'martenbt bold left');
							$HTML->clear();
							$HTML->tag('p', $row['team_a_name'], 'padtwenlt padtwenrt bglight lnfourty martwenbt');
														
							$FORM->score('team_a_score', $row['team_a_score']);
							$FORM->dropdownMenu('team_a_bonus', 'Award bonus point(s)', array(0,1), array('No', 'Yes'), $row['team_a_bonus'], FALSE);

							$HTML->tag('p', 'Participant 2 (away):', 'martenbt bold left');
							$HTML->clear();
							$HTML->tag('p', $row['team_b_name'], 'padtwenlt padtwenrt bglight lnfourty martwenbt');
							
							$FORM->score('team_b_score', $row['team_b_score']);
							$FORM->dropdownMenu('team_b_bonus', 'Award bonus point(s)', array(0,1), array('No', 'Yes'), $row['team_b_bonus'], FALSE);
							$FORM->datePicker('fixture_date', $row['fixture_date']);
							$FORM->hidden('fx_id', $row['fx_id']);
							$FORM->hidden('team_a', $row['team_a']);
							$FORM->hidden('team_b', $row['team_b']);
							
							$FORM->endForm('Edit fixture');
						
						endwhile;
						
					else:
						$HTML->errorMsg('Ooops, something seems to have gone wrong...');
					endif;
				else:
					$HTML->errorMsg('Ooops, something seems to have gone wrong...');
				endif;
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
			
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>