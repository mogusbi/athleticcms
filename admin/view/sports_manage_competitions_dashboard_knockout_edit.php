<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
			
				$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'Back', 'button right');
		
				$HTML->tag('h1', $page['competition'].' '.$page['season'], 'fsth');
				$HTML->tag('h2', $page['title'], 'fsfe');
				
				$HTML->clear();
			
				while($row = $MYSQL->FetchArray($check[3])):

					$FORM->startForm();
					$FORM->autocomplete($page['autocomplete'], 'inputstagename');
					$FORM->inputBox('stage_name', 'text', $row['stage_name']);
					$FORM->dropdownMenu('format', 'Format', array(0,1), array('Aggregate based', 'Best of series'), $row['format']);
					$FORM->inputBox('round', 'text', $row['round']);
					$FORM->hidden('rid', $row['rid']);
					$FORM->endForm('Edit knockout stage');
			
				endwhile;
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
			
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');		
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>