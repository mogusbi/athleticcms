<?php

if ($MYSQL->countRows($controller)):
	$HTML->adminLink('sports/manage/'.$url[2].'/seasons', 'Back', 'button right');
	
	$HTML->tag('h1', $page['title']);
	
	$FORM->startForm();
	$FORM->inputBox('season', 'text');
	$FORM->hidden('sport_id', $url[2]);
	$FORM->endForm('Add season');
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>