<?php

if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
			
				$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'Back', 'button right');
				
				$HTML->tag('h1', $page['competition'].' '.$page['season'], 'fsth');
				$HTML->tag('h2', $page['title'], 'fsfe');
				
				$HTML->clear();
				
				$HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/add', 'Add new tie', 'button left martwenrt');
				$HTML->clear();
				
?>
<table>
	<tr>
		<th colspan="3"></th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr<?php if ($row['fixture_check'] == '0') echo(' class="bglight"'); ?>>
		<td class="txtright">
			<?php 
				if ($row['format'] == '0'):
					// Aggregate format
					$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a_id'].'/dashboard/'.$url[6], $row['team_a'], 'link');
				else:
					// Series format
					if ($row['team_a_agg'] > $row['team_b_agg']):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a_id'].'/dashboard/'.$url[6], $row['team_a'], 'link');
					elseif($row['team_b_agg'] > $row['team_a_agg']):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b_id'].'/dashboard/'.$url[6], $row['team_b'], 'link');
					else:
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a_id'].'/dashboard/'.$url[6], $row['team_a'], 'link');
					endif;
				endif;
			?>
		</td>
		<td class="txtcen">
			<?php
				if ($row['format'] == '0'):
					// Aggregate format
					if (!empty($row['team_a_bonus'])):
						echo ('* ');
					endif;
						
					if ($row['fixture_check'] == '0'):
						echo ('-');
					else:
						echo ($row['team_a_agg'].' - '.$row['team_b_agg']);
					endif;
					
					if (!empty($row['team_b_bonus'])):
						echo (' *');
					endif;
				else:
					// Series format
					if ($row['team_a_agg'] == $row['team_b_agg']):
						echo ('-');
					else:
						echo ('bt');
					endif;
				endif;
			?>
		</td>
		<td>
			<?php 
				if ($row['format'] == '0'):
					// Aggregate format
					$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b_id'].'/dashboard/'.$url[6], $row['team_b'], 'link');
				else:
					// Series format
					if ($row['team_a_agg'] < $row['team_b_agg']):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a_id'].'/dashboard/'.$url[6], $row['team_a'], 'link');
					elseif($row['team_b_agg'] < $row['team_a_agg']):
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b_id'].'/dashboard/'.$url[6], $row['team_b'], 'link');
					else:
						$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b_id'].'/dashboard/'.$url[6], $row['team_b'], 'link');
					endif;
				endif;
			?>
		</td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$row['tie_id'], 'View', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$row['tie_id'].'/delete', $row['tie_id'], 'Delete', 'tie_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<p class="martwentp">* Denotes the award of bonus point(s)</p>
<?php
				
			else:
				$HTML->errorMsg('Ooops, something seems to have gone wrong...');
			endif;
		
		else:
			$HTML->errorMsg('Ooops, something seems to have gone wrong...');
		endif;
	else:
		$HTML->errorMsg('Ooops, something seems to have gone wrong...');
	endif;
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>