<?php

	$HTML->adminLink('users/add', 'Add new user', 'button left');
	$HTML->adminPagination('users', end($url), $controller[1]);
	
	$HTML->clear();
?>
<table>
	<tr>
		<th>Name</th>
		<th>Email address</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?=$row['surname']; ?>, <?=$row['first_name']; ?></td>
		<td><?=$row['email']; ?></td>
		<td>
			<?php
				$HTML->adminLink('users/edit/'.$row['user_id'], 'Edit', 'link');
			
				if ($row['user_id'] != $SESSION->userinfo['user_id']):
			?>
			&bull;
			<?php
					$FORM->deleteButton('users/delete/', $row['user_id'], 'Delete', 'user_id', 'link');
				endif;
			?>
		</td>
	</tr>
	<?php endwhile ?>
</table>