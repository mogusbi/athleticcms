<?php

if ($MYSQL->countRows($check[0])):

	$HTML->adminLink('sports/manage/'.$url[2].'/teams-players', 'Teams/players', 'button left martwenrt');
	$HTML->adminLink('sports/manage/'.$url[2].'/seasons', 'Seasons', 'button left martwenrt');
	$HTML->adminLink('sports/manage/'.$url[2].'/competitions', 'Competitions', 'button left');
	
	$HTML->clear();
	
	if ($MYSQL->countRows($controller[0])):
?>
<table>
	<tr>
		<th colspan="3"><?=$MYSQL->countRows($controller[0]); ?> result(s) due</th>
		<th>Date</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a'].'/dashboard/'.$row['season_id'], $row['team_a_name'], 'link'); ?></td>
		<td class="txtcen">-</td>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b'].'/dashboard/'.$row['season_id'], $row['team_b_name'], 'link'); ?></td>
		<td><?php $HTML->convertDate($row['fixture_date']); ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/competitions/'.$row['comp_id'].'/dashboard/'.$row['season_id'].'/'.$row['format'].'/'.$row['rid'].'/tie/'.$row['tie_id'].'/fixture/'.$row['fx_id'].'/edit', 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/competitions/'.$row['comp_id'].'/dashboard/'.$row['season_id'].'/'.$row['format'].'/'.$row['rid'].'/tie/'.$row['tie_id'].'/fixture/'.$row['fx_id'].'/delete', $row['fx_id'], 'Delete', 'fx_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<?php
	endif; 

	if ($MYSQL->countRows($controller[1])):
?>
<div class="left border-box fiftypcwide padtenrt">
	<table>
		<tr>
			<th colspan="3">Upcoming fixture(s)</th>
			<th class="padzero">Date</th>
		</tr>
		<?php while($row = $MYSQL->FetchArray($controller[1])): ?>
	<tr>
		<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a'].'/dashboard/'.$row['season_id'], $row['team_a_name'], 'link'); ?></td>
		<td class="txtcen fivewide padzero">-</td>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b'].'/dashboard/'.$row['season_id'], $row['team_b_name'], 'link'); ?></td>
		<td class="padzero"><?php $HTML->convertDate($row['fixture_date']); ?></td>
	</tr>
	<?php endwhile; ?>
	</table>
</div>
<?php
	endif; 

	if ($MYSQL->countRows($controller[2])):
		if ($MYSQL->countRows($controller[1]) == 0):
?>
<div>
<?php else: ?>
<div class="right border-box fiftypcwide padtenlt">
<?php endif; ?>
	<table>
		<tr>
			<th colspan="3">Latest result(s)</th>
			<th class="padzero">Date</th>
		</tr>
		<?php while($row = $MYSQL->FetchArray($controller[2])): ?>
	<tr>
		<td class="txtright"><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_a'].'/dashboard/'.$row['season_id'], $row['team_a_name'], 'link'); ?></td>
		<td class="txtcen padzero"><?=$row['team_a_score']; ?> - <?=$row['team_b_score']; ?></td>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_b'].'/dashboard/'.$row['season_id'], $row['team_b_name'], 'link'); ?></td>
		<td class="padzero"><?php $HTML->convertDate($row['fixture_date']); ?></td>
	</tr>
	<?php endwhile; ?>
	</table>
</div>
<?php endif; ?>
<div class="clear"></div>
<table>
	<tr>
		<th colspan="2"><?=$page['title']; ?> statistics</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[3])): ?>
	<tr>
		<td class="bold">Teams/players</td>
		<td class="txtcen"><?=$row['teams_players']; ?></td>
	</tr>
	<tr>
		<td class="bold">Fixtures</td>
		<td class="txtcen"><?=$row['fixtures']; ?></td>
	</tr>
	<tr>
		<td class="bold">Competitions</td>
		<td class="txtcen"><?=$row['competitions']; ?></td>
	</tr>
	<tr>
		<td class="bold">Seasons</td>
		<td class="txtcen"><?=$row['seasons']; ?></td>
	</tr>
	<?php endwhile; ?>
</table>
<?php

else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;
?>