<?php

if ($MYSQL->countRows($check)):

	$HTML->tag('h1', $page['title'], 'left');
	
	$HTML->adminLink('sports/manage/'.$url[2].'/teams-players', 'Back', 'button right');
	
	$HTML->clear();
	
	$HTML->adminPagination('sports/manage/'.$url[2].'/teams-players', end($url), $controller[1]);
	
	$HTML->clear();

?>
<table>
	<tr>
		<th>Select season</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$url[4].'/dashboard/'.$row['season_id'], $row['season'], 'link'); ?></td>
	</tr>
	<?php endwhile ?>
</table>
<?php

else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>