<?php

if ($MYSQL->countRows($check)):
	$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/add', 'Add new team/player', 'button left');
	$HTML->adminLink('sports/manage/'.$url[2], 'Back', 'button right martwenbt');
	
	$HTML->clear();
	
	$HTML->adminPagination('sports/manage/'.$url[2].'/teams-players', end($url), $controller[1]);
	
	$HTML->clear();
?>
<table>
	<tr>
		<th>Name</th>
		<th>Actions</th>
	</tr>
	<?php while($row = $MYSQL->FetchArray($controller[0])): ?>
	<tr>
		<td><?=$HTML->adminLink('sports/manage/'.$url[2].'/teams-players/'.$row['team_id'].'/season-select', $row['name'], 'link'); ?></td>
		<td>
			<?php $HTML->adminLink('sports/manage/'.$url[2].'/teams-players/edit/'.$row['team_id'], 'Edit', 'link'); ?>
			&bull;
			<?php $FORM->deleteButton('sports/manage/'.$url[2].'/teams-players/delete', $row['team_id'], 'Delete', 'team_id', 'link'); ?>
		</td>
	</tr>
	<?php endwhile; ?>
</table>
<?php
else:
	$HTML->errorMsg('Ooops, something seems to have gone wrong...');
endif;

?>