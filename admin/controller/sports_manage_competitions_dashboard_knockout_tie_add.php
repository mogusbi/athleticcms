<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants
$check[2] = $MYSQL->Select('competition_participants', '*', 'WHERE season_id = "'.$url[6].'" AND comp_id = "'.$url[4].'"');

// Check round we're editing exists
$check[3] = $MYSQL->Select('competition_stages', '*', 'WHERE comp_id = "'.$url[4].'" AND rid = "'.$url[8].'"');

// Get ties
$controller[0] = $MYSQL->Query('SELECT a.tie_id, b.name AS team_a, b.team_id AS team_a_id, c.name AS team_b, c.team_id AS team_b_id FROM competition_tie a
INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)
WHERE tie_id = "'.$url[10].'" AND comp_id = "'.$url[4].'" AND rid = "'.$url[8].'" AND season_id = "'.$url[6].'"
ORDER BY a.tie_id ASC');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;

while($row = $MYSQL->FetchArray($check[3])):
	$page['stage'] = $row['stage_name'];
endwhile;

$page['team_name'] = array();
$page['team_id'] = array();

while($row = $MYSQL->FetchArray($controller[0])):
	$page['tie'] = $row['team_a'].' vs. '.$row['team_b'];
	
	array_push($page['team_name'], $row['team_a']);
	array_push($page['team_id'], $row['team_a_id']);
	array_push($page['team_name'], $row['team_b']);
	array_push($page['team_id'], $row['team_b_id']);
endwhile;

$page['title'] = 'Add new fixture';

// Forms
if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	if ($MYSQL->countRows($check[0])):
		if ($MYSQL->countRows($check[1])):
				
			if ($MYSQL->countRows($check[2])):
				if ($MYSQL->countRows($check[3])):
					if ($MYSQL->countRows($controller[0])):
					
						// Error checker
						$errorMsg = array();
					
						// Clean submitted values
						$FORM->cleanUp($_POST);
						
						// Begin transaction
						$MYSQL->transactionStart();
						
						// Insert fixture
						$insert['tie'] = $MYSQL->Insert('fixtures', 'tie_id, team_a, team_b, fixture_date', '"'.$_POST['tie_id'].'", "'.$_POST['team_a'].'", "'.$_POST['team_b'].'", "'.$_POST['fixture_date'].'"');
						
						// Check all has gone well and carry on or else rollback
						if ($insert['tie'] !== TRUE):
							$MYSQL->transactionRollback();
							array_push($errorMsg, 'Fixture could not be added');
						else:
						
							// Get fixture ID
							$insert['fx_id'] = $MYSQL->getInsertID();
							
							if ($insert['fx_id'] == 0):
								array_push($errorMsg, 'Fixture could not be found. Process aborted');
							else:
							
								// Check if we are inserting a score or a NULL for if fixture hasn't been played yet
								if($_POST['team_a_score'] == '0' || $_POST['team_b_score'] == '0'):
									$_POST['team_a_score'] = '"'.$_POST['team_a_score'].'"';
									$_POST['team_b_score'] = '"'.$_POST['team_b_score'].'"';
								else:
									if (empty($_POST['team_a_score']) && empty($_POST['team_b_score'])):
										$_POST['team_a_score'] = 'NULL';
										$_POST['team_b_score'] = 'NULL';
									else:
										$_POST['team_a_score'] = '"'.$_POST['team_a_score'].'"';
										$_POST['team_b_score'] = '"'.$_POST['team_b_score'].'"';
									endif;
								endif;

								
								// Insert scores
								$insert['scores'] = $MYSQL->Query('INSERT INTO fixture_scores (fx_id, team_id, score, bonus) VALUES ("'.$insert['fx_id'].'", "'.$_POST['team_a'].'", '.$_POST['team_a_score'].', "'.$_POST['team_a_bonus'].'"), ("'.$insert['fx_id'].'", "'.$_POST['team_b'].'", '.$_POST['team_b_score'].', "'.$_POST['team_b_bonus'].'")');
								
								if ($insert['scores'] !== TRUE):
									$MYSQL->transactionRollback();
									array_push($errorMsg, 'Scores could not be added');
								endif;
							
							endif;
						
						endif;
													
						$MYSQL->transactionEnd();
						
						// Redirect
						if (!empty($errorMsg)):
							$message = 'The following error(s) have been encountered: ';
							$message .= implode(', ', $errorMsg);
							$message .= '. Please try again';
							
							$FORM->adminRedirect('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10], $message, 'error');
						else:
							$FORM->adminRedirect('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10], 'Fixture added');
						endif;
						
					endif;
				endif;
			endif;
		endif;
	endif;
endif;

?>