<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	$FORM->processForm($_POST, 'fixtures', 'delete', 'sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/knockout/'.$url[8].'/tie/'.$url[10], 'Fixture removed');
endif;

$page['title'] = 'Delete fixture';

?>