<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants and get their names
$check[2] = $MYSQL->Query('SELECT a.team_id, b.name FROM competition_participants a
INNER JOIN teams b ON (a.team_id = b.team_id)
WHERE a.season_id = "'.$url[6].'" AND a.comp_id = "'.$url[4].'"
ORDER BY b.name ASC');

// Check round we're editing exists
$check[3] = $MYSQL->Select('competition_stages', '*', 'WHERE comp_id = "'.$url[4].'" AND rid = "'.$url[8].'"');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;

$page['team_name'] = array();
$page['team_id'] = array();

while($row = $MYSQL->FetchArray($check[2])):
	array_push($page['team_name'], $row['name']);
	array_push($page['team_id'], $row['team_id']);
endwhile;

while($row = $MYSQL->FetchArray($check[3])):
	$page['title'] = $row['stage_name'];
endwhile;

// Forms
if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	// Add round if all is in order
	if ($MYSQL->countRows($check[0])):
		if ($MYSQL->countRows($check[1])):
			if ($MYSQL->countRows($check[3])):
				$FORM->processForm($_POST, 'competition_tie', 'insert', 'sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8], 'Added new tie');
			endif;
		endif;
	endif;
endif;

?>