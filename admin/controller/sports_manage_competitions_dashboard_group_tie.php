<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants
$check[2] = $MYSQL->Select('competition_participants', '*', 'WHERE season_id = "'.$url[6].'" AND comp_id = "'.$url[4].'"');

// Check round we're editing exists
$check[3] = $MYSQL->Select('competition_stages', '*', 'WHERE comp_id = "'.$url[4].'" AND rid = "'.$url[8].'"');

// Get ties
$controller[0] = $MYSQL->Query('SELECT a.tie_id, b.name AS team_a, b.team_id AS team_a_id, c.name AS team_b, c.team_id AS team_b_id FROM competition_tie a
INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)
WHERE tie_id = "'.$url[10].'"
ORDER BY a.tie_id ASC');

// Get fixtures
$controller[1] = $MYSQL->Query('SELECT a.fx_id, a.team_a, b.name AS team_a_name, d.score AS team_a_score, d.bonus AS team_a_bonus, a.team_b, c.name AS team_b_name, e.score AS team_b_score, e.bonus AS team_b_bonus, a.fixture_date FROM fixtures a
INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)
INNER JOIN fixture_scores d ON (a.fx_id = d.fx_id AND a.team_a = d.team_id)
INNER JOIN fixture_scores e ON (a.fx_id = e.fx_id AND a.team_b = e.team_id)
WHERE a.tie_id = "'.$url[10].'"
ORDER BY a.fixture_date ASC');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;

while($row = $MYSQL->FetchArray($check[3])):
	$page['stage'] = $row['stage_name'];
endwhile;

while($row = $MYSQL->FetchArray($controller[0])):
	$page['title'] = $row['team_a'].' vs. '.$row['team_b'];
endwhile;

?>