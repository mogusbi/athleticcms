<?php

// Title
$page['title'] = 'Dashboard';

// Check for overdue results
$controller[0] = $MYSQL->Query('SELECT a.fx_id, a.team_a, b.name AS team_a_name, a.team_b, c.name AS team_b_name, a.fixture_date, g.comp_id, h.sport_id, f.tie_id, f.rid, f.season_id,

(CASE
	WHEN (g.format = "2") THEN "group" ELSE "knockout"
END) AS format

FROM fixtures a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

INNER JOIN fixture_scores d ON (a.fx_id = d.fx_id AND a.team_a = d.team_id AND d.score IS NULL)
INNER JOIN fixture_scores e ON (a.fx_id = e.fx_id AND a.team_b = e.team_id AND d.score IS NULL)

INNER JOIN competition_tie f ON (a.tie_id = f.tie_id)
INNER JOIN competition_stages g ON (f.rid = g.rid)
INNER JOIN competitions h ON (g.comp_id = h.comp_id)

WHERE a.fixture_date <= CURDATE()

ORDER BY a.fixture_date ASC');

// Get upcoming fixtures
$controller[1] = $MYSQL->Query('SELECT a.fx_id, a.team_a, b.name AS team_a_name, a.team_b, c.name AS team_b_name, a.fixture_date, g.comp_id, h.sport_id, f.tie_id, f.rid, f.season_id,

(CASE
	WHEN (g.format = "2") THEN "group" ELSE "knockout"
END) AS format

FROM fixtures a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

INNER JOIN fixture_scores d ON (a.fx_id = d.fx_id AND a.team_a = d.team_id AND d.score IS NULL)
INNER JOIN fixture_scores e ON (a.fx_id = e.fx_id AND a.team_b = e.team_id AND d.score IS NULL)

INNER JOIN competition_tie f ON (a.tie_id = f.tie_id)
INNER JOIN competition_stages g ON (f.rid = g.rid)
INNER JOIN competitions h ON (g.comp_id = h.comp_id)

WHERE a.fixture_date >= CURDATE()

ORDER BY a.fixture_date ASC, a.fx_id DESC

LIMIT 0,10');

// Get latest results
$controller[2] = $MYSQL->Query('SELECT a.fx_id, a.team_a, b.name AS team_a_name, d.score AS team_a_score, a.team_b, c.name AS team_b_name, e.score AS team_b_score, a.fixture_date, g.comp_id, h.sport_id, f.tie_id, f.rid, f.season_id,

(CASE
	WHEN (g.format = "2") THEN "group" ELSE "knockout"
END) AS format

FROM fixtures a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

INNER JOIN fixture_scores d ON (a.fx_id = d.fx_id AND a.team_a = d.team_id AND d.score IS NOT NULL)
INNER JOIN fixture_scores e ON (a.fx_id = e.fx_id AND a.team_b = e.team_id AND d.score IS NOT NULL)

INNER JOIN competition_tie f ON (a.tie_id = f.tie_id)
INNER JOIN competition_stages g ON (f.rid = g.rid)
INNER JOIN competitions h ON (g.comp_id = h.comp_id)

WHERE a.fixture_date <= CURDATE()

ORDER BY a.fixture_date DESC, a.fx_id DESC

LIMIT 0,10');

// Stats
$controller[3] = $MYSQL->Query('
SELECT
	(
		SELECT COUNT(comp_id) FROM competitions
	)
	AS competitions,
	(
		SELECT COUNT(team_id) FROM teams
	)
	AS teams_players,
	(
		SELECT COUNT(season_id) FROM seasons
	)
	AS seasons,
	(
		SELECT COUNT(a.fx_id) FROM fixtures a
		
		INNER JOIN competition_tie b ON (a.tie_id = b.tie_id)
		INNER JOIN competitions c ON (b.comp_id = c.comp_id)
	)
	AS fixtures,
	(
		SELECT COUNT(user_id) FROM users
	)
	AS users,
	(
		SELECT COUNT(sport_id) FROM sports
	)
	AS sports
');

?>