<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	$FORM->processForm($_POST, 'competition_stages', 'delete', 'sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'Knockout stage removed');
endif;

$page['title'] = 'Delete knockout stage';

?>