<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	$FORM->processForm($_POST, 'seasons', 'delete', 'sports/manage/'.$url[2].'/seasons', 'Season removed');
endif;

$page['title'] = 'Delete season';

?>