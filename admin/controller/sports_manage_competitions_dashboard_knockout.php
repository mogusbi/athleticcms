<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants
$check[2] = $MYSQL->Select('competition_participants', '*', 'WHERE season_id = "'.$url[6].'" AND comp_id = "'.$url[4].'"');

// Check round we're editing exists
$check[3] = $MYSQL->Select('competition_stages', '*', 'WHERE comp_id = "'.$url[4].'" AND rid = "'.$url[8].'"');

// Get ties and aggregate results
$controller[0] = $MYSQL->Query('SELECT a.tie_id, b.name AS team_a, b.team_id AS team_a_id, c.name AS team_b, c.team_id AS team_b_id, SUM(e.bonus) AS team_a_bonus, SUM(f.bonus) AS team_b_bonus, g.format, COUNT(d.fx_id) AS fixture_check,

SUM(CASE
	WHEN (a.team_a = e.team_id AND g.format = "0") THEN e.score
	WHEN (a.team_a = e.team_id AND g.format = "1" AND e.score > f.score) THEN 1 ELSE 0
END) AS team_a_agg,

SUM(CASE 
	WHEN (a.team_b = f.team_id AND g.format = "0") THEN f.score
	WHEN (a.team_b = f.team_id AND g.format = "1" AND f.score > e.score) THEN 1 ELSE 0
END) as team_b_agg

FROM competition_tie a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

LEFT JOIN fixtures d ON (a.tie_id = d.tie_id)
LEFT JOIN fixture_scores e ON (d.fx_id = e.fx_id AND a.team_a = e.team_id)
LEFT JOIN fixture_scores f ON (d.fx_id = f.fx_id AND a.team_b = f.team_id)

INNER JOIN competition_stages g ON (g.comp_id = "'.$url[4].'" AND g.rid = "'.$url[8].'")

WHERE a.comp_id = "'.$url[4].'" AND a.rid = "'.$url[8].'" AND a.season_id = "'.$url[6].'"

GROUP BY a.tie_id

ORDER BY a.tie_id ASC');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;

while($row = $MYSQL->FetchArray($check[3])):
	$page['title'] = $row['stage_name'];
endwhile;

?>