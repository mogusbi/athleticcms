<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants
$check[2] = $MYSQL->Select('competition_participants', '*', 'WHERE season_id = "'.$url[6].'" AND comp_id = "'.$url[4].'"');

// Get teams
$controller[0] = $MYSQL->Select('teams', 'team_id, name', 'WHERE sport_id = "'.$url[2].'"');

// Get stages
$controller[1] = $MYSQL->Select('competition_stages', 'rid, stage_name, round, format', 'WHERE comp_id = "'.$url[4].'" ORDER BY round ASC, stage_name ASC');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
	$page['participants'] = $row['participants'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;


$page['team_names'] = array();
$page['team_ids'] = array();

while($row = $MYSQL->FetchArray($controller[0])):
	array_push($page['team_ids'], $row['team_id']);
	array_push($page['team_names'], $row['name']);
endwhile;

// Forms
if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	// Add participants
	if ($MYSQL->countRows($check[0])):
		if ($MYSQL->countRows($check[1])):
			if ($MYSQL->countRows($check[2]) == 0):
				foreach($_POST['team_id'] as &$team):
					$submit = $MYSQL->Insert('competition_participants', 'comp_id, season_id, team_id', '"'.$url[4].'", "'.$url[6].'", "'.$team.'"');
				endforeach;
				
				if (!isset($_SESSION['error']))
					$FORM->adminRedirect('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'Participants added');
			endif;
		endif;
	endif;
endif;

// Title
$page['title'] = $page['competition'];

?>