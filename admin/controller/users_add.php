<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	
	// Hash password
	$_POST['salt'] = md5(mt_rand().time());
	
	if (!empty($_POST['password'])):
		$_POST['password'] = md5(md5($_POST['salt']).md5($_POST['password']));
	endif;
	
	$FORM->processForm($_POST, 'users', 'insert', 'users', 'New user added to the system');
endif;

$page['title'] = 'Add new user';

?>