<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	$FORM->processForm($_POST, 'teams', 'delete', 'sports/manage/'.$url[2].'/teams-players', 'Team/player removed');
endif;

$page['title'] = 'Delete team/player';

?>