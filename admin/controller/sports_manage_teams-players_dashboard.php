<?php

// Check that team/player exists
$check[0] = $MYSQL->Select('teams', '*', 'WHERE sport_id = "'.$url[2].'" AND team_id ="'.$url[4].'"');

// Check that season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Get team/player data
while($row = $MYSQL->FetchArray($check[0])):
	$page['title'] = $row['name'];
endwhile;

// Season stats
$controller[0] = 'SELECT a.team_a, a.team_b, b.score, c.score, e.team_id, e.name AS team,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score IS NOT NULL) OR (a.team_b = e.team_id AND c.score IS NOT NULL) THEN 1 ELSE 0
END) AS pld,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score IS NOT NULL) THEN 1 ELSE 0
END) AS home_pld,

SUM(CASE
	WHEN (a.team_b = e.team_id AND c.score IS NOT NULL) THEN 1 ELSE 0
END) AS away_pld,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score > c.score) OR (a.team_b = e.team_id AND b.score < c.score) THEN 1 ELSE 0
END) AS wins,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score > c.score) THEN 1 ELSE 0
END) AS home_wins,

SUM(CASE
	WHEN (a.team_b = e.team_id AND b.score < c.score) THEN 1 ELSE 0
END) AS away_wins,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score < c.score) OR (a.team_b = e.team_id AND b.score > c.score) THEN 1 ELSE 0
END) AS defeats,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score < c.score) THEN 1 ELSE 0
END) AS home_defeats,

SUM(CASE
	WHEN (a.team_b = e.team_id AND b.score > c.score) THEN 1 ELSE 0
END) AS away_defeats,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score = c.score) OR (a.team_b = e.team_id AND b.score = c.score) THEN 1 ELSE 0
END) as draws,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score = c.score) THEN 1 ELSE 0
END) as home_draws,

SUM(CASE
	WHEN (a.team_b = e.team_id AND b.score = c.score) THEN 1 ELSE 0
END) as away_draws,

COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN b.score
	WHEN (a.team_b = e.team_id) THEN c.score
END), 0) AS score_for,

COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN b.score
END), 0) AS home_score_for,

COALESCE(SUM(CASE 
	WHEN (a.team_b = e.team_id) THEN c.score
END), 0) AS away_score_for,

COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN c.score
	WHEN (a.team_b = e.team_id) THEN b.score
END), 0) AS score_against,

COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN c.score
END), 0) AS home_score_against,

COALESCE(SUM(CASE 
	WHEN (a.team_b = e.team_id) THEN b.score
END), 0) AS away_score_against,

COALESCE(SUM(CASE
	WHEN (a.team_a = e.team_id) THEN b.score
	WHEN (a.team_b = e.team_id) THEN c.score
END)
-
SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN c.score
	WHEN (a.team_b = e.team_id) THEN b.score
END), 0) AS score_difference

FROM fixtures a

INNER JOIN fixture_scores b ON (a.fx_id = b.fx_id AND a.team_a = b.team_id)
INNER JOIN fixture_scores c ON (a.fx_id = c.fx_id AND a.team_b = c.team_id)

INNER JOIN competition_tie d ON (a.tie_id = d.tie_id AND d.season_id = "'.$url[6].'"';

if (isset($url[7])):
	$controller[0] .= ' AND d.comp_id = "'.$url[7].'"';
endif;

$controller[0] .= ')

INNER JOIN teams e ON (a.team_a = e.team_id) OR (a.team_b = e.team_id)


WHERE e.team_id = "'.$url[4].'"

GROUP BY e.team_id';

$controller[0] = $MYSQL->Query($controller[0]);

while($row = $MYSQL->FetchArray($controller[0])):
	$page['wins'] = $row['wins'];
	$page['home_wins'] = $row['home_wins'];
	$page['away_wins'] = $row['away_wins'];
	
	$page['draws'] = $row['draws'];
	$page['home_draws'] = $row['home_draws'];
	$page['away_draws'] = $row['away_draws'];
	
	$page['defeats'] = $row['defeats'];
	$page['home_defeats'] = $row['home_defeats'];
	$page['away_defeats'] = $row['away_defeats'];
	
	$page['pld'] = $row['pld'];
	$page['home_pld'] = $row['home_pld'];
	$page['away_pld'] = $row['away_pld'];
	
	$page['score_for'] = $row['score_for'];
	$page['home_score_for'] = $row['home_score_for'];
	$page['away_score_for'] = $row['away_score_for'];
	
	$page['score_against'] = $row['score_against'];
	$page['home_score_against'] = $row['home_score_against'];
	$page['away_score_against'] = $row['away_score_against'];
endwhile;

// Get fixtures
$controller[1] = 'SELECT a.fx_id, a.team_a, b.name AS team_a_name, a.team_b, c.name AS team_b_name, a.fixture_date, g.comp_id, f.tie_id, f.rid,

(CASE
	WHEN (g.format = "2") THEN "group" ELSE "knockout"
END) AS format

FROM fixtures a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

INNER JOIN fixture_scores d ON (a.fx_id = d.fx_id AND a.team_a = d.team_id AND d.score IS NULL)
INNER JOIN fixture_scores e ON (a.fx_id = e.fx_id AND a.team_b = e.team_id AND e.score IS NULL)

INNER JOIN competition_tie f ON (a.tie_id = f.tie_id';

if (isset($url[7])):
	$controller[1] .= ' AND f.comp_id = "'.$url[7].'"';
endif;

$controller[1] .= ')
INNER JOIN competition_stages g ON (f.rid = g.rid';

if (!isset($url[7])):
	$controller[1] .= ' AND f.comp_id = g.comp_id';
endif;

$controller[1] .= ')

WHERE (a.team_a = "'.$url[4].'" OR a.team_b = "'.$url[4].'") AND f.season_id = "'.$url[6].'" AND a.fixture_date >= CURDATE()
ORDER BY a.fixture_date ASC
LIMIT 0, 5';

$controller[1] = $MYSQL->Query($controller[1]);

// Get results
$controller[2] = 'SELECT a.fx_id, a.team_a, b.name AS team_a_name, d.score AS team_a_score, d.bonus AS team_a_bonus, a.team_b, c.name AS team_b_name, e.score AS team_b_score, e.bonus AS team_b_bonus, a.fixture_date, g.comp_id, f.tie_id, f.rid,

(CASE
	WHEN (g.format = "2") THEN "group" ELSE "knockout"
END) AS format

FROM fixtures a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

INNER JOIN fixture_scores d ON (a.fx_id = d.fx_id AND a.team_a = d.team_id AND d.score IS NOT NULL)
INNER JOIN fixture_scores e ON (a.fx_id = e.fx_id AND a.team_b = e.team_id AND e.score IS NOT NULL)

INNER JOIN competition_tie f ON (a.tie_id = f.tie_id';

if (isset($url[7])):
	$controller[2] .= ' AND f.comp_id = "'.$url[7].'"';
endif;

$controller[2] .= ')
INNER JOIN competition_stages g ON (f.rid = g.rid';

if (!isset($url[7])):
	$controller[2] .= ' AND f.comp_id = g.comp_id';
endif;

$controller[2] .= ')

WHERE (a.team_a = "'.$url[4].'" OR a.team_b = "'.$url[4].'") AND f.season_id = "'.$url[6].'" AND a.fixture_date <= CURDATE()
ORDER BY a.fixture_date DESC
LIMIT 0, 5';

$controller[2] = $MYSQL->Query($controller[2]);

// Get competitions
$controller[3] = 'SELECT a.competition_name, a.comp_id

FROM competitions a

INNER JOIN competition_participants b ON (a.comp_id = b.comp_id AND b.team_id = "'.$url[4].'" AND b.season_id = "'.$url[6].'")
';

if (isset($url[7])):
	$controller[3] .= 'WHERE a.comp_id = "'.$url[7].'"
	';
endif;

$controller[3] .= 'ORDER BY a.competition_name ASC';

$controller[3] = $MYSQL->Query($controller[3]);

if (isset($url[7])):
	while($row = $MYSQL->FetchArray($controller[3])):
		$page['competition'] = $row['competition_name'];
	endwhile;
endif;

?>