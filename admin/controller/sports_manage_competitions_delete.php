<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	$FORM->processForm($_POST, 'competitions', 'delete', 'sports/manage/'.$url[2].'/competitions', 'Competition removed');
endif;

$page['title'] = 'Delete competition';

?>