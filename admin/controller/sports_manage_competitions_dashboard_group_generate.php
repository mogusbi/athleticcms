<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants and get their names
$check[2] = $MYSQL->Query('SELECT a.team_id, b.name FROM competition_participants a
INNER JOIN teams b ON (a.team_id = b.team_id)
WHERE a.season_id = "'.$url[6].'" AND a.comp_id = "'.$url[4].'"
ORDER BY b.name ASC');

// Check round we're editing exists
$check[3] = $MYSQL->Select('competition_stages', '*', 'WHERE comp_id = "'.$url[4].'" AND rid = "'.$url[8].'"');

$page['title'] = 'Generate ties';


// Batch process ties
if ($MYSQL->countRows($check[0])):
	if ($MYSQL->countRows($check[1])):
		if ($MYSQL->countRows($check[2])):
			if ($MYSQL->countRows($check[3])):
			
				// Team list
				$team_a = array();
				$team_b = array();
				
				while($row = $MYSQL->FetchArray($check[2])):
					array_push($team_a, $row['team_id']);
					array_push($team_b, $row['team_id']);
				endwhile;
				
				$count = 0;
				$ties = 0;
				
				// Begin transaction
				$MYSQL->transactionStart();
				
				foreach($team_a as $home):
				
					unset($team_b[$count]);
					
					foreach($team_b as $away):
						$insert = $MYSQL->Insert('competition_tie', 'team_a, team_b, comp_id, rid, season_id', '"'.$team_a[$count].'", "'.$away.'", "'.$url[4].'", "'.$url[8].'", "'.$url[6].'"');
						
						if ($insert !== TRUE):
							$MYSQL->transactionRollback();
							$message = 'Ties could not be added';
						else:
							$ties++;
						endif;
					endforeach;
					
					$count++;
					
				endforeach;
				
				// Completet transaction
				$MYSQL->transactionEnd();
				
				// Redirect
				if (!empty($errorMsg)):
					$FORM->adminRedirect('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8], $message, 'error');
				else:
					$FORM->adminRedirect('sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6].'/group/'.$url[8], $ties.' ties added');
				endif;
			
			endif;
		endif;
	endif;
endif;

?>