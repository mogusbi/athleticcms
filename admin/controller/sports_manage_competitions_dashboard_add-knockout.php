<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants
$check[2] = $MYSQL->Select('competition_participants', '*', 'WHERE season_id = "'.$url[6].'" AND comp_id = "'.$url[4].'"');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;

// Get autocomplete values for round names (helps to keep names consistent between different competitions)
$controller[0] = $MYSQL->Select('competition_stages', 'DISTINCT(stage_name) AS stage_name', 'ORDER BY stage_name ASC');

$page['autocomplete'] = array();

while($row = $MYSQL->FetchArray($controller[0])):
	array_push($page['autocomplete'], '"'.$row['stage_name'].'"');
endwhile;

// Forms
if ($_SERVER['REQUEST_METHOD'] === 'POST'):	
	// Add round if all is in order
	if ($MYSQL->countRows($check[0])):
		if ($MYSQL->countRows($check[1])):
			if ($MYSQL->countRows($check[2])):
				$FORM->processForm($_POST, 'competition_stages', 'insert', 'sports/manage/'.$url[2].'/competitions/'.$url[4].'/dashboard/'.$url[6], 'New knockout stage added to the system');
			endif;
		endif;
	endif;
endif;

// Title
$page['title'] = 'Add new knockout stage';

?>