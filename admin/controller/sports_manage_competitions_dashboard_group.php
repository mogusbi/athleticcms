<?php

// Check competition exists
$check[0] = $MYSQL->Select('competitions', '*', 'WHERE sport_id = "'.$url[2].'" AND comp_id = "'.$url[4].'"');

// Check season exists
$check[1] = $MYSQL->Select('seasons', '*', 'WHERE season_id = "'.$url[6].'" AND sport_id = "'.$url[2].'"');

// Check participants
$check[2] = $MYSQL->Select('competition_participants', '*', 'WHERE season_id = "'.$url[6].'" AND comp_id = "'.$url[4].'"');

// Check round we're editing exists
$check[3] = $MYSQL->Select('competition_stages', '*', 'WHERE comp_id = "'.$url[4].'" AND rid = "'.$url[8].'"');

// Get ties
$controller[0] = $MYSQL->Query('SELECT a.tie_id, b.name AS team_a, b.team_id AS team_a_id, c.name AS team_b, c.team_id AS team_b_id, COUNT(d.fx_id) AS fixture_check

FROM competition_tie a

INNER JOIN teams b ON (a.team_a = b.team_id)
INNER JOIN teams c ON (a.team_b = c.team_id)

LEFT JOIN fixtures d ON (a.tie_id = d.tie_id)

WHERE a.comp_id = "'.$url[4].'" AND a.rid = "'.$url[8].'" AND a.season_id = "'.$url[6].'"

GROUP BY a.tie_id

ORDER BY a.tie_id ASC');

// Generate table
$controller[1] = $MYSQL->Query('SELECT a.team_a, a.team_b, b.score, c.score, e.team_id, e.name AS team,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score IS NOT NULL) OR (a.team_b = e.team_id AND c.score IS NOT NULL) THEN 1 ELSE 0
END) AS pld,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score > c.score) OR (a.team_b = e.team_id AND b.score < c.score) THEN 1 ELSE 0
END) AS wins,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score < c.score) OR (a.team_b = e.team_id AND b.score > c.score) THEN 1 ELSE 0
END) AS defeats,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score = c.score) OR (a.team_b = e.team_id AND b.score = c.score) THEN 1 ELSE 0
END) as draws,

SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score > c.score) OR (a.team_b = e.team_id AND b.score < c.score) THEN f.win_points ELSE 0
END)
+
SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score = c.score) OR (a.team_b = e.team_id AND b.score = c.score) THEN f.draw_points ELSE 0
END)
+
SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score < c.score) OR (a.team_b = e.team_id AND b.score > c.score) THEN f.loss_points ELSE 0
END)
+
SUM(CASE
	WHEN (a.team_a = e.team_id AND b.bonus = "1") OR (a.team_b = e.team_id AND c.bonus = "1") THEN g.bonus_points ELSE 0
END) AS pts,

COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN b.score
	WHEN (a.team_b = e.team_id) THEN c.score
END), 0) AS score_for,

COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN c.score
	WHEN (a.team_b = e.team_id) THEN b.score
END), 0) AS score_against,

COALESCE(SUM(CASE
	WHEN (a.team_a = e.team_id) THEN b.score
	WHEN (a.team_b = e.team_id) THEN c.score
END)
-
SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN c.score
	WHEN (a.team_b = e.team_id) THEN b.score
END), 0) AS score_difference,

COALESCE(ROUND(COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN b.score
	WHEN (a.team_b = e.team_id) THEN c.score
END), 0)
/
SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score IS NOT NULL) OR (a.team_b = e.team_id AND c.score IS NOT NULL) THEN 1 ELSE 0
END), 1), 0) AS average_score_for,

COALESCE(ROUND(COALESCE(SUM(CASE 
	WHEN (a.team_a = e.team_id) THEN c.score
	WHEN (a.team_b = e.team_id) THEN b.score
END), 0)
/
SUM(CASE
	WHEN (a.team_a = e.team_id AND b.score IS NOT NULL) OR (a.team_b = e.team_id AND c.score IS NOT NULL) THEN 1 ELSE 0
END), 1), 0) AS average_score_against

FROM fixtures a

INNER JOIN fixture_scores b ON (a.fx_id = b.fx_id AND a.team_a = b.team_id)
INNER JOIN fixture_scores c ON (a.fx_id = c.fx_id AND a.team_b = c.team_id)

INNER JOIN competition_tie d ON (a.tie_id = d.tie_id AND d.comp_id = "'.$url[4].'" AND d.rid = "'.$url[8].'" AND d.season_id = "'.$url[6].'")

INNER JOIN teams e ON (a.team_a = e.team_id) OR (a.team_b = e.team_id)

INNER JOIN competition_stages f ON (f.comp_id = "'.$url[4].'" AND f.rid = "'.$url[8].'")

INNER JOIN competitions g ON (g.comp_id = "'.$url[4].'")

GROUP BY e.team_id

ORDER BY pts DESC, average_score_for DESC, score_difference DESC, wins DESC, draws DESC, team ASC');

// Grab some values
while($row = $MYSQL->FetchArray($check[0])):
	$page['competition'] = $row['competition_name'];
endwhile;

while($row = $MYSQL->FetchArray($check[1])):
	$page['season'] = $row['season'];
endwhile;

while($row = $MYSQL->FetchArray($check[3])):
	$page['title'] = $row['stage_name'];
endwhile;

?>