<?php
/**
* @file index.php
* @brief Athletic CMS admininistration control panel
* @author Mohamed Gusbi
* @date 20th January 2013
* @version 1.0
*/

/* Sessions */
session_start();

/* Load config files */
@require_once('../conf/conf.php');

/* Load components */
@require_once('../components/admin.php');

?>
<!doctype html>
<html lang="<?=$CONF['lang']; ?>">
<head>
<meta charset="UTF-8">
<title><?=$page['title']; ?> &laquo; AthleticCMS</title>

<!-- Styles -->
<link href="<?=$CONF['url']; ?>common/admin.css" rel="stylesheet" type="text/css" />
<!-- Styles -->

<!-- Icons -->
<link href="<?=$CONF['url']; ?>admin/favicon.ico" rel="icon" type="image/x-icon"/>
<!-- Icons -->

<!-- JavaScript -->
<script src="<?=$CONF['url']; ?>common/jquery.min.js" type="text/javascript"></script>
<script src="<?=$CONF['url']; ?>common/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?=$CONF['url']; ?>common/highcharts.js" type="text/javascript"></script>
<script src="<?=$CONF['url']; ?>common/admin.js" type="text/javascript"></script>
<!-- JavaScript -->
</head>

<body>
<!-- Header -->
<div class="bgdark">
	<div class="header onetwofivehigh padtwenlt padtwenrt">
		<a href="<?=$CONF['admin']; ?>index/" class="lnonetwofive font-white fsfe cabin semi-bold left notransition">Athletic<span class="fsth font-blue">cms</span></a>
		<p class="lnonetwofive right font-white fsfo">Welcome back <?php $HTML->adminLink('users/edit/'.$SESSION->userinfo['user_id'], $SESSION->userinfo['first_name'], 'link notransition'); ?> &bull; <?php $HTML->adminLink('logout', 'Log out', 'link notransition'); ?></p>
	</div>
	
	<div class="clear"></div>
	
	<ul class="bglighter thirtyfivehigh padtwenlt padtwenrt">
		<li class="left marfivert"><a href="<?=$CONF['admin']; ?>index/" class="navbutton<?php if ($url[0] == 'index') echo(' nav-selected'); ?>">Dashboard</a></li>
		<?php
			foreach ($menu as $item):
				if ($url[0] == strtolower($item)):
					$class = 'navbutton nav-selected';
				else:
					$class = 'navbutton';
				endif;
		?>
		<li class="left marfivert"><?php $HTML->adminLink(strtolower($item), $item, $class); ?></li>
		<?php
			endforeach;
		?>
	</ul>
	<?php if ($url[0] == 'sports'): ?>
	<ul class="bgblue thirtyfivehigh padtwenlt padtwenrt">
		<?php
			$submenu = $MYSQL->select('sports', '*');
			
			while($link = $MYSQL->FetchArray($submenu)):
				if ($link['sport_id'] == $url[2]):
					$class = 'navbutton subnav-selected';
				else:
					$class = 'navbutton subnavbutton';
				endif;
		?>
		<li class="left marfivert"><?php $HTML->adminLink('sports/manage/'.$link['sport_id'], $link['sport'], $class); ?></li>
		<?php		
			endwhile;
		?>
	</ul>
	<?php endif; ?>
</div>
<!-- Header -->

<!-- Content -->
<div class="padtwen bgwhite">
	<?php
		if (isset($_SESSION['msg']) && !isset($_SESSION['error']))
			$HTML->msg($_SESSION['msg']);
			
		if (isset($_SESSION['error']))
			$HTML->errorMsg($_SESSION['error']);
	?>
	<div class="clear"></div>
	<?php
		/* Display page */		
		if (file_exists('view/'.$file))
			@require_once('view/'.$file);
		else
			$HTML->errorMsg('The page you are looking for doesn\'t exist');
	?>
	<div class="clear"></div>
</div>
<!-- Content -->

<!-- Footer -->
<div class="bglighter thirtyfivehigh padtwenlt padtwenrt font-white">
	<p class="left cabin fsfo">Developed by <a href="http://mogusbi.co.uk" target="_blank" class="strong lnthirtyfive">mo<span class="font-blue">gusbi</span></a></p>
</div>
<!-- Footer -->
</body>
</html>
<?php
	if (isset($_SESSION['msg']))
		unset($_SESSION['msg']);
		
	if (isset($_SESSION['error']))
		unset($_SESSION['error']);
?>