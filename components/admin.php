<?php
/*
*
* Admin panel components file
* Started: 20th January 2013
*
*/

//error_reporting(E_ALL);

/* Load neccessary component files */
@require_once('files/class.mysql.php');
@require_once('files/class.html.php');
@require_once('files/class.form.php');
@require_once('files/class.session.php');

/* Declare components */
$MYSQL = new MySQL($CONF);
$HTML = new HTML($CONF);
$FORM = new Form($CONF);
$SESSION = new Session($CONF);

/* URLS */
if (empty($_SERVER['PATH_INFO']))
	$url = explode( '/', strip_tags($_SERVER['ORIG_PATH_INFO']));
else
	$url = explode( '/', strip_tags($_SERVER['PATH_INFO']));

array_shift($url);
$url = array_filter($url);

if (!isset($url[0]))
	$url[0] = 'index';

/* Load individual page controller */
$file = $url;
		
foreach ($file as $key => $value):
	if (preg_match('/[0-9]/', $value))
		unset($file[$key]);
endforeach;

$file = str_replace('_.php', '.php', implode('_', $file).'.php');

if (file_exists('controller/'.$file))
	@require_once('controller/'.$file);
else
	$page['title'] = 'Error';
	
/* Set main menu */
$menu = array('Users', 'Sports');
	
/* Session management */
if (isset($_COOKIE['userkey'])):
	$SESSION->assignInfo($_COOKIE['userkey']);
	
	/* Stop signed in users from seeing the log in form again */
	if ($url[0] == 'login'):
		$FORM->adminRedirect('index', 'You cannot access that page', 'error');
	endif;
	
else:
	if ($url[0] != 'login'):
		$FORM->adminRedirect('login', 'You must be logged in to view this page', 'error');
	endif;
endif;

/* Load login page */
if ($url[0] == 'login'):
	@require_once('view/'.$file);
	exit();
endif;

?>