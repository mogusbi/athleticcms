<?php
/**
* @file class.html.php
* @brief HTML class file
*
* @class HTML
* @author Mohamed Gusbi
* @date 20th January 2013
* @version 1.0
* @brief This class is used to quickly generate commonly
* used HTML objects and blocks such as hyperlinks,
* paginations and error messages
*/

class HTML {
	public function __construct($CONF) {
		$this->CONF = $CONF;
	}
	
	/**
	* @brief Used to display error messages on a page
	* @param $msg The message to be displayed
	*/
	public function errorMsg($msg) {
		echo('<div class="bgred font-white padtwenlt padtwenlt fourtyhigh lnfourty martwenbt"><span class="bold underline">Error:</span> '.$msg.'</div>');
	}
	
	/**
	* @brief Used to display messages on a page such as confirmation that an action has been successfully completed
	* @param $msg The message to be displayed
	*/
	public function msg($msg) {
		echo('<div class="bggreen font-white padtwenlt padtwenlt fourtyhigh lnfourty martwenbt"><span class="bold underline">Message:</span> '.$msg.'</div>');
	}
	
	/**
	* @brief Places a clear div tag rather than having to manually add the HTML code using echo every time
	*/
	public function clear() {
		echo('<div class="clear"></div>');
	}
	
	/**
	* @brief Automatically generates full hyperlinks on the system
	* @param $location The location of where you wish to link to relative to the root of the site
	* @param $label The text for the hyperlink
	* @param $class CSS class(es) to be applied to the A tag. Ignore if no style is required
	*/
	public function hyperlink($location, $label, $class = NULL) {
		echo('<a href="'.$this->CONF['url'].$location.'/"');
		
		if (isset($class))
			echo (' class="'.$class.'"');
		
		echo('>'.$label.'</a>');
	}
	
	/**
	* @brief Automatically generates full hyperlinks for the admin control panel
	* @param $location The location of where you wish to link to relative to the admin panel
	* @param $label The text for the hyperlink
	* @param $class CSS class(es) to be applied to the A tag. Ignore if no style is required
	*/
	public function adminLink($location, $label, $class = NULL) {
		echo('<a href="'.$this->CONF['admin'].$location.'/"');
		
		if (isset($class))
			echo (' class="'.$class.'"');
		
		echo('>'.$label.'</a>');
	}
	
	/**
	* @brief Automatically generates pagination elements such as page numbers, next and previous page buttons
	* @param $location The location of where you wish to link to relative to the admin panel
	* @param $position The current page we are on. This should be automatically generated from our URL
	* @param $lastpage The number of our last page. This should be automatically generated from a SQL query
	*/
	public function adminPagination($location, $position, $lastpage) {
		$prevpage = $position - 1;
		$nextpage = $position + 1;
		
		echo('<div class="pagination right fourtyhigh lnfourty">');
		
		if ($lastpage != 0):
			if($position != 1)
				self::adminLink($location.'/'.$prevpage, '&laquo;', 'button marfivert left');
				
			if ($lastpage != 1):
				for($i = 1; $i <= $lastpage; $i++):
					if ($i == $position)
						$selectedclass = ' bold bgblue font-white';
					else
						$selectedclass = ' lightbutton';
						
					self::adminLink($location.'/'.$i, $i, 'fourtyhigh lnfourty fourtywide dspblock txtcen marfivert left'.$selectedclass);
				endfor;
			endif;
			
			if($position != $lastpage)
				self::adminLink($location.'/'.$nextpage, '&raquo;', 'button left');
		endif;
		
		echo('</div>');
	}
	
	/**
	* @brief Converts mySQL date string into a more user friendly format
	* @param $input The mySQL date string
	*/
	public function convertDate($input) {
		echo date($this->CONF['date_format'], strtotime($input));
	}
	
	/**
	* @brief Quickly generate an html tag on the page such as a heading tag or a paragraph tag
	* @param $tag Your HTML tag of choice such as h1, h2, etc. or p
	* @param $content The text content to go inbetween your tags
	* @param $classes Any CSS class you with to assign this html tag
	*/
	public function tag($tag, $content, $classes = NULL) {
		echo ('<'.$tag);
		
		if ($classes):
			echo (' class="'.$classes.'"');
		endif;
		
		echo ('>'.$content);
		
		echo ('</'.$tag.'>');
	}
}

?>