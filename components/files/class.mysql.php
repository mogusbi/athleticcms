<?php
/**
* @file class.mysql.php
* @brief mySQL class file
*
* @class MySQL
* @author Mohamed Gusbi
* @date 20th January 2013
* @version 2.1
* @brief This class is used to handle and execute mySQL queries
*/

class MySQL {
	private static 
	$connected = false,
	$connection = false;
	
	public function __construct($CONF) {
		$this->CONF = $CONF;
		
		if (!self::$connected):
			self::$connection = mysqli_connect($this->CONF['host'], $this->CONF['user'], $this->CONF['pass'], $this->CONF['dbse']);
			
			mysqli_set_charset(self::$connection, 'utf8');
			
			if(!self::$connection)
				die('Cannot connect to the database');
			else
				self::$connected = true;
		endif;
	}
	
	public function __deconstruct() {
		if (self::$connected):
			mysqli_close(self::$connection);
		endif;
	}
	
	/**
	 * @brief Executes a mySQL query
	 * @param $query The complete mySQL query to be executed
	 * @return The result from the executed query
	*/
	public static function query($query) {
		if (self::$connected):
			$query = mysqli_query(self::$connection, $query)
				or $_SESSION['error'] = mysqli_error(self::$connection);
				
			return $query;
		endif;
	}
	
	/**
	 * @brief Executes an input mySQL query using defined paramaters
	 * @param $table The table you wish to insert into
	 * @param $cols The columns in the defined table you wish to insert data into
	 * @param $values The values you wish to insert
	 * @return The result from the executed query
	*/
	public static function insert($table, $cols, $values) {
		if (self::$connected):
			$query = mysqli_query(self::$connection, 'INSERT INTO '.$table.' ('.$cols.') VALUES ('.$values.')')
				or $_SESSION['error'] = mysqli_error(self::$connection);
				
			return $query;
		endif;
	}
	
	/**
	 * @brief Performs a simple SELECT query
	 * @param $tables The table you wish to query
	 * @param $cols The colums you wish to get values from
	 * @param $xtra Any constraints you may wish to add to the SELECT query
	 * @return The result from the executed query
	*/
	public static function select($table, $cols, $xtra = NULL) {
		if (self::$connected):
			if ($xtra):
				$query = mysqli_query(self::$connection, 'SELECT '.$cols.' FROM '.$table.' '.$xtra)
					or $_SESSION['error'] = mysqli_error(self::$connection);
			else:
				$query = mysqli_query(self::$connection, 'SELECT '.$cols.' FROM '.$table)
					or $_SESSION['error'] = mysqli_error(self::$connection);
			endif;
				
			return $query;
		endif;
	}
	
	/**
	 * @brief When used in conjunction with adminPagination generates pagination for pages with lists
	 * @param $tables The table you wish to query
	 * @param $cols The colums you wish to get values from
	 * @param $position The current page we are on. This should be automatically generated from our URL
	 * @param $xtra Any constraints you may wish to add to the SELECT query
	 * @return The result from the executed query
	 */
	public static function paginationSelect($table, $cols, $limit, $position, $xtra = NULL) {
		if (self::$connected):
			if ($xtra):
				$counter = mysqli_query(self::$connection, 'SELECT '.$cols.' FROM '.$table.' '.$xtra);
					
				$limitvalue = $position * $limit - ($limit);
				$lastpage = ceil(self::countRows($counter)/$limit);
					
				$query = mysqli_query(self::$connection, 'SELECT '.$cols.' FROM '.$table.' '.$xtra.' LIMIT '.$limitvalue.', '.$limit);
			else:
				$query = mysqli_query(self::$connection, 'SELECT '.$cols.' FROM '.$table);
			endif;

			return array($query, $lastpage);	
		endif;
	}
	
	/**
	 * @brief Performs a simple UPDATE query
	 * @param $tables The table you wish to query
	 * @param $cols The colums you wish to update
	 * @param $where Constraints used to update the correct row, e.g. id = '1'
	 * @return The result from the executed query
	 */
	public static function update($table, $cols, $where) {
		if (self::$connected):
			$query = mysqli_query(self::$connection, 'UPDATE '.$table.' SET '.$cols.' WHERE '.$where)
				or $_SESSION['error'] = mysqli_error(self::$connection);
				
			return $query;
		endif;
	}
	
	/**
	 * @brief Performs a simple DELETE query
	 * @param $tables The table you wish to query
	 * @param $where Constraints used to delete the correct row, e.g. id = '1'
	 * @return The result from the executed query
	*/
	public static function delete($table, $where) {
		if (self::$connected):
			$query = mysqli_query(self::$connection, 'DELETE FROM '.$table.' WHERE '.$where)
				or $_SESSION['error'] = mysqli_error(self::$connection);
				
			return $query;
		endif;
	}
	
	/**
	 * @brief Puts the results from a query into an array
	 * @param $query The returned result from a query
	 */
	public static function fetchArray($query) {
		if (self::$connected):
			$array = mysqli_fetch_array($query, MYSQLI_ASSOC);
			return $array;
		endif;
	}
	
	/**
	 * @brief Counts the number of rows in a table
	 * @param $query The returned result from a query. This should ideally be from a SELECT query
	 */
	public static function countRows($query) {
		if (self::$connected):
			$num = mysqli_num_rows($query);
			return $num;
		endif;
	}
	
	/**
	 * @brief Displays the number of rows changed after a query
	 * @param $query The returned result from a query
	 */
	public static function countUpdated($query) {
		if (self::$connected):
			$num = mysqli_affected_rows(self::$connection);
			return $num;
		endif;
	}
	
	/**
	 * @brief Begins a transaction block
	 */
	public static function transactionStart() {
		return mysqli_autocommit(self::$connection, FALSE);
	}
	
	/**
	 * @brief Commits a transaction after a block
	 */
	public static function transactionEnd() {
		return mysqli_commit(self::$connection);
	}
	
	/**
	 * @brief Rolls back the database to it's previous state
	 */
	public static function transactionRollback() {
		return mysqli_rollback(self::$connection);
	}
	
	/**
	 * @brief Returns the ID of last insert
	 */
	public static function getInsertID() {
		$num = mysqli_insert_id(self::$connection);
		return $num;
	}
}

?>