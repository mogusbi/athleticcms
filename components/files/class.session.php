<?php
/**
* @file class.session.php
* @brief Session class file
*
* @class Session
* @author Mohamed Gusbi
* @date 21st January 2013
* @version 1.1
* @brief This class is used to manage sessions on AthleticCMS
*/

class Session extends MySQL {
	
	/**
	* @brief Manages the signing in process
	* @param $email Email address that a user has input
	* @param $password The password a user has input
	* @param $location The location to redirect to after a user has successfully signed in. The URL MUST be absolute
	*/
	public function logIn($email, $password, $location) {		
		$email = trim(htmlentities(strip_tags($email)));
		$password = trim(htmlentities(strip_tags($password)));
		$location = str_replace('login', 'index', trim(htmlentities(strip_tags($location))));
		
		$login = parent::select('users', 'password, salt', 'WHERE email = "'.$email.'"');
		
		if (parent::countRows($login) == 0):
			
			// User doesn't exist
			$_SESSION['error'] = 'Incorrect email address';
		else:
			while($user = parent::fetchArray($login)):
				if ($user['password'] == md5(md5($user['salt']).md5($password))):
					
					// Username and password are correct, assign userkey cookie
					setcookie('userkey', $user['salt'], time()+2592000, '/');
					header('location: '.$location);
				else:
					// Incorrect password
					$_SESSION['error'] = 'Incorrect password';
				endif;
			endwhile;
		endif;
	}
	
	/**
	* @brief Signs a user out of the system
	*/
	public function logOut() {
		setcookie('userkey', '', time()+3600, '/');
	}
	
	/**
	* @param $userinfo User class variable. This holds all user data once they are signed into the system
	*/
	var $userinfo = array();
	
	/**
	* @brief Assigns user information into a variable to be used elsewhere on the system
	* @param $userkey User password salt/key used to retrieve their information. This value should be coming from the userkey cookie set during the sign in process
	*/	
	public function assignInfo($userkey) {
		$login = parent::select('users', 'user_id, first_name, surname, email', 'WHERE salt = "'.$userkey.'"');
		
		while($user = parent::fetchArray($login)):
			$this->userinfo = array(
				'user_id' => $user['user_id'],
				'first_name' => $user['first_name'],
				'surname' => $user['surname'],
				'email' => $user['email']
			);
		endwhile;
	}
}

?>