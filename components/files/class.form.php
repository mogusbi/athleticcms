<?php
/**
* @file class.form.php
* @brief Form class file
*
* @class Form
* @author Mohamed Gusbi
* @date 21st January 2013
* @version 1.0
* @brief This class is used to quickly generate forms, 
* input boxes of various types, form validation 
* as well as processing these forms once they are submitted
*/

class Form extends MySQL {

	public function __construct($CONF) {
		$this->CONF = $CONF;
	}
	
	/**
	* @brief Generate opening form tag
	*/
	public function startForm() {
		echo('<form method="post" novalidate="novalidate">');
	}
	
	/**
	* @brief Generate submit button and closing form tag
	* @param $button The text on the submit button
	*/
	public function endForm($button) {
		echo('<input type="submit" value="'.$button.'" />
		</form>');
	}
	
	/**
	* @brief Generate input box and it's label
	* @param $name The name of the input box. This should be the same as the name of the column the inputted data will go into
	* @param $type The type of input box this will be (i.e. text, password, email[html5], etc.). Do not use for hidden input boxes, use the hidden method instead
	* @param $value The initial value inside the text box
	*/
	public function inputBox($name, $type, $value = NULL) {
		echo('<div class="inputwrap martwenbt">
			<label for="input'.str_replace('_', '', $name).'" class="martenbt bold left curpointer">'.ucfirst(str_replace('_', ' ', $name)).':</label>
			<div class="error right font-red dspnone">Error message</div>
			<div class="clear"></div>
			<div class="padtwenlt padtwenrt bglight">
				<input type="'.$type.'" id="input'.str_replace('_', '', $name).'" autocomplete="off" class="lnfourty fourtyhigh bglight" name="'.$name.'"');
		
		if (isset($value))
			echo(' value="'.$value.'"');
		
		echo(' data-validation="true" />
			</div>
		</div>');
	}
	
	/**
	* @brief Generate special input box for scores
	* @param $name The name of the input box
	* @param $value The initial value inside the text box
	*/
	public function score($name, $value = NULL) {
		echo('<div class="inputwrap martwenbt marminustwentp right">
			<label for="input'.str_replace('_', '', $name).'" class="lnfourty martenbt martenrt bold left curpointer">Score:</label>
			<div class="padtwenlt padtwenrt bglight right fiftywide">
				<input type="number" id="input'.str_replace('_', '', $name).'" autocomplete="off" class="lnfourty fourtyhigh bglight fiftywide txtcen" name="'.$name.'"');
		
		if (isset($value))
			echo(' value="'.$value.'"');
		
		echo(' data-validation="false" />
			</div>
		</div>
		<div class="clear"></div>');
	}
	
	/**
	* @brief Generate datepicker input box
	* @param $name The name of the input box. This should be the same as the name of the column the inputted data will go into
	* @param $value The initial value inside the text box
	*/
	public function datePicker($name, $value = NULL) {
		echo('<div class="inputwrap martwenbt">
			<label for="input'.str_replace('_', '', $name).'" class="martenbt bold left curpointer">'.ucfirst(str_replace('_', ' ', $name)).':</label>
			<div class="error right font-red dspnone">Error message</div>
			<div class="clear"></div>
			<div class="padtwenlt padtwenrt bglight">
				<script type="text/javascript">
					$(function() {
						$("#input'.str_replace('_', '', $name).'").datepicker({ dateFormat: "yy-mm-dd", changeMonth: true, changeYear: true }).val();
					});
				</script>
				<input type="text" id="input'.str_replace('_', '', $name).'" autocomplete="off" class="lnfourty fourtyhigh bglight" name="'.$name.'"');
		
		if (isset($value))
			echo(' value="'.$value.'"');
		
		echo(' data-validation="true" />
			</div>
		</div>');
	}
	
	/**
	* @brief Generate drop down box and it's values
	* @param $name The name of the input box. This should be the same as the name of the column the inputted data will go into
	* @param $displayname The label name used for this drop down box
	* @param $values An array of the values of each option
	* @param $display An array of the labels for each option
	* @param $selected Preset the selected value
	* @param $validation Whether or not you want this drop down box to be checked for duplicate values
	*/
	public function dropdownMenu($name, $displayname, $values, $display, $selected = NULL, $validation = TRUE) {
		echo ('<div class="inputwrap martwenbt">
			<label for="input'.str_replace('_', '', $displayname).'" class="martenbt bold left curpointer">'.ucfirst(str_replace('_', ' ', $displayname)).':</label>
			<div class="error right font-red dspnone">Error message</div>
			<div class="clear"></div>
			<div class="padtwenlt padtwenrt bglight">
				<select name="'.$name.'" class="lnfourty fourtyhigh bglight" id="input'.str_replace('_', '', $displayname).'"' );
		
		if ($validation == FALSE)
			echo(' data-validation="false"');
		else
			echo(' data-validation="true"');
		
		echo ('>
		');
		
		$loop = 0;
		
		foreach($display as &$disp):
			echo ('<option value="'.$values[$loop].'"');
			
			if ($selected == $values[$loop])
				echo (' selected="selected"');
			
			echo ('>'.$disp.'</option>');
			
			$loop++;
		endforeach;
		
		echo ('
				</select>
			</div>
		</div>');
	}
	
	/**
	* @brief Generates an autocomplete list for input boxes. Note: Each instance can only be used on one input box
	* @param $values The values that will go into the autocomplete list. Must be an array
	* @param $id The ID of the input box that will use this autocomplete list
	*/
	public function autocomplete($values, $id) {
		echo ('<script type="text/javascript">
		$(function() {
			var availableTags = [');
		
		echo implode(',', $values);
		
		echo ('];
			$( "#'.$id.'" ).autocomplete({
				source: availableTags
			});
		});
		</script>');
	}
	
	/**
	* @brief Generate hidden input box
	* @param $name The name of the input box. This should be the same as the name of the column the inputted data will go into
	* @param $value The initial value inside the text box
	*/
	public function hidden($name, $value) {
		echo('<input type="hidden" name="'.$name.'" value="'.$value.'" />');
	}
	
	/**
	* @brief Validates form data and then inserts into database or updates row in the database.
	* Validation is there as a back up in case the end user has disabled JavaScript, which is the first port of call for form validation in the system.
	* If the form being processed is more complicated than a simple INSERT or UPDATE, then you will be required to manually write up the processing code in the relevant controller file
	* @param $input The input values from the form in array form. It is best to have $_POST as the value of this paramater
	* @param $table The SQL table the input data will go into
	* @param $action Determines what to do with the form. Values MUST be either insert, update or delete, anything else will bring up an error
	* @param $location The location to go to after the form has been processed. The location MUST be relative to the administration control panel
	* @param $message The message to be shown when form has been successfully processed
	*/
	public function processForm($input, $table, $action, $location, $message) {
		// Form validation
		$errorMsg = array();
		
		foreach($input as $key => $value):
			if (empty($input[$key])):
				if ($input[$key] != '0'):
					array_push($errorMsg, ucfirst(str_replace('_', ' ', $this->cleanUp($key))));
				endif;
			endif;
		endforeach;
		
		if (!empty($errorMsg)):
			$message = 'The following field(s) (';
			$message .= implode(', ', $errorMsg);
			$message .= ') were input incorrectly. Please try again';
			
			$this->adminRedirect($location, $message, 'error');
		endif;
		
		// Everything seems valid, carry on....
		$names = array();
		$values = array();
		
		foreach($input as $key => $value):
			array_push($names, $this->cleanUp($key));
			array_push($values, '"'.$this->cleanUp($input[$key]).'"');
		endforeach;
		
		if ($action == 'insert'):
			parent::insert($table, implode(', ', $names), implode(',', $values));
		elseif ($action == 'update'):
			$update = array();
			
			foreach($names as $key => $value):
				array_push($update, $names[$key].' = '.$values[$key]);
			endforeach;
			
			parent::update($table, implode(', ', $update), end($update));
		elseif ($action == 'delete'):
			parent::delete($table, implode(', ', $names).' = '.implode(',', $values));
		else:
			$this->adminRedirect($location, 'It was not specified what exactly you wished to do with this form', 'error');
		endif;
			
		$this->adminRedirect($location, $message);
	}
	
	/**
	* @brief Sanitise all input values
	* @param $data Input data to be sanitised
	* @return The value of our paramater after it has been sanitised
	*/
	public function cleanUp($data) {
		$data = trim(htmlentities(strip_tags($data), ENT_COMPAT, 'UTF-8'));
		return $data;
	}
	
	/**
	* @brief Redirects to another page within the administration control panel and the message to show once you have been redirected
	* @param $location The location to redirect to using PHP, the location MUST be relative to the administration control panel
	* @param $message The message to show after redirect has been carried out
	* @param $type The type of message to show. Ignore if it is just a regular message, use the value error if you wish to show an error message
	*/
	public function adminRedirect($location, $message, $type = 'normal') {
		if ($type == 'normal')
			$_SESSION['msg'] = $message;
		else if ($type == 'error')
			$_SESSION['error'] = $message;
		
		header('location: '.$this->CONF['admin'].$location.'/');
		exit();
	}
	
	/**
	* @brief Generates button to delete something
	* @param $location The location to the script that will process the delete. Path must be relative to the administration control panel
	* @param $id The ID of the row you wish to delete. This should be automatically generated from a SQL query to avoid deleting the wrong thing
	* @param $label The text to appeaer on the delete link. Something such as 'Delete' will usually do
	* @param $param The name of the hidden input box. Should be the name of the table column you are targetting
	* @param $class CSS class(es) to be applied to the A tag. Ignore if no style is required
	*/
	public function deleteButton($location, $id, $label, $param, $class = NULL) {
		echo('<form action="'.$this->CONF['admin'].$location.'" method="post" name="form_'.$id.'" id="form_'.$id.'" class="dspnone"><input type="hidden" name="'.$param.'" value="'.$id.'" /></form>
			<a href="#" onclick="confirmDelete(\'form_'.$id.'\'); return false;"');
		
		if (isset($class))
			echo (' class="'.$class.'"');
		
		echo('>'.$label.'</a>');
	}
}

?>