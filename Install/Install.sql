-- Generation Time: May 26, 2013 at 05:09 PM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure for table `competitions`
--

CREATE TABLE `competitions` (
  `comp_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` int(11) NOT NULL,
  `competition_name` varchar(50) NOT NULL,
  `participants` int(11) NOT NULL,
  `bonus_points` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`comp_id`),
  UNIQUE KEY `comp` (`competition_name`),
  KEY `sport_id_idx` (`sport_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `competition_participants`
--

CREATE TABLE `competition_participants` (
  `part_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  PRIMARY KEY (`part_id`),
  UNIQUE KEY `comp_id` (`comp_id`,`season_id`,`team_id`),
  KEY `comp_id_idx` (`comp_id`),
  KEY `season_id_idx` (`season_id`),
  KEY `team_id_idx` (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `competition_stages`
--

CREATE TABLE `competition_stages` (
  `rid` int(11) NOT NULL AUTO_INCREMENT,
  `comp_id` int(11) NOT NULL,
  `stage_name` varchar(50) NOT NULL,
  `round` int(11) NOT NULL,
  `win_points` int(11) DEFAULT NULL,
  `draw_points` int(11) DEFAULT NULL,
  `loss_points` int(11) DEFAULT NULL,
  `format` int(11) DEFAULT NULL,
  PRIMARY KEY (`rid`),
  UNIQUE KEY `STAGE` (`comp_id`,`stage_name`),
  KEY `comp_id_idx` (`comp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `competition_tie`
--

CREATE TABLE `competition_tie` (
  `tie_id` int(11) NOT NULL AUTO_INCREMENT,
  `comp_id` int(11) NOT NULL,
  `season_id` int(11) NOT NULL,
  `rid` int(11) NOT NULL,
  `team_a` int(11) NOT NULL,
  `team_b` int(11) NOT NULL,
  PRIMARY KEY (`tie_id`),
  UNIQUE KEY `TIE` (`comp_id`,`rid`,`team_a`,`team_b`,`season_id`),
  KEY `comp_id_idx` (`comp_id`),
  KEY `competition_tie_team_a_idx` (`team_a`),
  KEY `competition_tie_team_b_idx` (`team_b`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fixtures`
--

CREATE TABLE `fixtures` (
  `fx_id` int(11) NOT NULL AUTO_INCREMENT,
  `tie_id` int(11) NOT NULL,
  `team_a` int(11) NOT NULL,
  `team_b` int(11) NOT NULL,
  `fixture_date` date NOT NULL,
  PRIMARY KEY (`fx_id`),
  KEY `tie_id_idx` (`tie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `fixture_scores`
--

CREATE TABLE `fixture_scores` (
  `score_id` int(11) NOT NULL AUTO_INCREMENT,
  `fx_id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `score` int(11) DEFAULT NULL,
  `bonus` int(11) NOT NULL,
  PRIMARY KEY (`score_id`),
  KEY `fx_id_idx` (`fx_id`),
  KEY `fixture_scores_team_id_idx` (`team_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `seasons`
--

CREATE TABLE `seasons` (
  `season_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` int(11) NOT NULL,
  `season` varchar(20) NOT NULL,
  PRIMARY KEY (`season_id`),
  UNIQUE KEY `SEASON` (`sport_id`,`season`),
  KEY `sport_id_idx` (`sport_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sports`
--

CREATE TABLE `sports` (
  `sport_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport` varchar(50) NOT NULL,
  PRIMARY KEY (`sport_id`),
  UNIQUE KEY `sport` (`sport`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL AUTO_INCREMENT,
  `sport_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`team_id`),
  UNIQUE KEY `NAME` (`sport_id`,`name`),
  KEY `sport_id_idx` (`sport_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `surname` varchar(50) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Create Users
--

INSERT INTO `users` (`user_id`, `email`, `password`, `salt`, `first_name`, `surname`) VALUES
(1, 'M.Gusbi@2010.hull.ac.uk', '3193327a54e93ab626c89891b0164d48', '8adce7e735ebc1faeb9d97bc26154cfd', 'Mo', 'Gusbi'),
(2, 'test@user.com', '1556b8da1c9712d87fe7fff7cb419604', '3f32b4efc3c8e96db723f89743fd90a5', 'Test', 'User');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `competitions`
--
ALTER TABLE `competitions`
  ADD CONSTRAINT `competitions_sport_id` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`sport_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `competition_participants`
--
ALTER TABLE `competition_participants`
  ADD CONSTRAINT `competition_participants_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `competitions` (`comp_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `competition_participants_season_id` FOREIGN KEY (`season_id`) REFERENCES `seasons` (`season_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `competition_participants_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `competition_stages`
--
ALTER TABLE `competition_stages`
  ADD CONSTRAINT `competition_stages_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `competitions` (`comp_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `competition_tie`
--
ALTER TABLE `competition_tie`
  ADD CONSTRAINT `competition_tie_comp_id` FOREIGN KEY (`comp_id`) REFERENCES `competition_stages` (`rid`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `competition_tie_team_a` FOREIGN KEY (`team_a`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `competition_tie_team_b` FOREIGN KEY (`team_b`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fixtures`
--
ALTER TABLE `fixtures`
  ADD CONSTRAINT `fixtures_tie_id` FOREIGN KEY (`tie_id`) REFERENCES `competition_tie` (`tie_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `fixture_scores`
--
ALTER TABLE `fixture_scores`
  ADD CONSTRAINT `fixture_scores_fx_id` FOREIGN KEY (`fx_id`) REFERENCES `fixtures` (`fx_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fixture_scores_team_id` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `seasons`
--
ALTER TABLE `seasons`
  ADD CONSTRAINT `seasons_sport_id` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`sport_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teams`
--
ALTER TABLE `teams`
  ADD CONSTRAINT `teams_sport_id` FOREIGN KEY (`sport_id`) REFERENCES `sports` (`sport_id`) ON DELETE CASCADE ON UPDATE CASCADE;